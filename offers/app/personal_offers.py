# -*- coding: utf-8 -*-
import csv
import sys
import io

reload(sys)
sys.setdefaultencoding('UTF8')


class PersonalOffers:
    def __init__(self, dictionary, offers):
        self.offers = dict();
        self.users = dict();
        self.load_dictionary(dictionary)
        self.load_users(offers);

    def load_dictionary(self, dictionary):
        with io.open(dictionary, encoding='utf8') as csvfile:
            rows = list(csv.reader(csvfile))
            for row in rows:
                if row[0] == "Город":
                    continue

                if len(row) > 1:
                    offer = dict()
                    key = row[1];

                    meta = row[2].split(":")
                    if len(meta) != 2:
                        continue
                    offer["priority"] = row[0];
                    offer["title"] = meta[0].strip()
                    offer["description"] = meta[1].strip()
                    offer["picture"] = row[3]
                    offer["replic"] = row[5]
                    self.offers[key] = offer

    def load_users(self, offers):
        with io.open(offers, encoding='utf8') as csvfile:
            rows = list(csv.reader(csvfile))
            for row in rows:
                if row[0] == "Город":
                    continue

                if len(row) > 1:
                    user = dict()
                    key = row[2];
                    user["city"] = row[0]
                    user["user"] = row[1][:-4]

                    offers = list()
                    for index in range(13):
                        offer = row[index + 3].strip();
                        if len(offer) > 0:
                            if (offer in self.offers):
                                offers.append(self.offers[offer])

                    offers.sort(key=lambda k: int(k['priority']));
                    user["offers"] = offers[:3]
                    self.users[key[:-2]] = user;

    def offer_type(self, id):
        if id in self.offers:
            return self.offers[id];
        else:
            return dict()


    def get_offers(self, phone):
        if phone in self.users:
            return self.users[phone];
        else:
            return dict()
