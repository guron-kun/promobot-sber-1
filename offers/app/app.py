from flask import Flask, request, jsonify, make_response

from personal_offers import PersonalOffers

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

service = PersonalOffers("dictionary.csv", "offers.csv")

@app.route("/")
def offers():
    phone = request.args.get('phone', '')
    response = make_response(jsonify(service.get_offers(phone)))
    response.headers["Content-Type"] = "application/json; charset=utf-8"
    response.headers["access-control-allow-origin"] = "*"
    return response