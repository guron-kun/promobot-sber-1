module.exports = function (gulp, plugins, paths, path) {

    return function () {
        gulp.src(paths.pug.data+'**/*.json')
            .pipe(plugins.mergeJson({
                fileName: 'data.json',
                edit: function(json, file) {

                    // Extract the filename and strip the extension
                    let filename = path.basename(file.path),
                        primaryKey = filename.replace(path.extname(filename), '');

                    // Set the filename as the primary key for our JSON data
                    let data = {};
                    //data[primaryKey.toUpperCase()] = json;
                    data[primaryKey] = json;

                    return data;
                }
            }))
            .pipe(gulp.dest(paths.pug.buildData));
    };
};