module.exports = function (gulp, plugins, paths, path) {
    const cssnano = require('cssnano');
    const autoprefixer = require('autoprefixer');
    const normalize = require('postcss-normalize');
    return function () {
        gulp.src(paths.scss+'**/*.scss')
            .pipe(plugins.sass.sync().on('error', plugins.sass.logError))
            .pipe(plugins.postcss([
                normalize(),
                autoprefixer(),
                cssnano({
                    safe: true
                })
            ]))
            .pipe(gulp.dest(paths.public.css));
    };
};