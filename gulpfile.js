
const paths = {
    gulp: './gulp',
    pug: {
        tmpl: 'src/tmpl/',
        parts: 'src/tmpl/parts/',
        pages: 'src/pages/',
        popup: 'src/popup/',
        base: './',
        data: 'src/data/',
        buildData: 'gulp/temp/'
    },
    public:{
        html: 'public/',
        css: 'public/css/',
        img: 'public/img/',
        js: 'public/js/'
    },
    scss: 'src/scss/',
    img: 'src/img/'
};

const gulp = require('gulp');
const path = require('path');
const watch = require('gulp-watch');
const imagemin = require('gulp-imagemin');
const plugins = require('gulp-load-plugins')();

function getTask(task) {
    return require(`./gulp/${task}`)(gulp, plugins, paths, path);
}

gulp.task('pug-data', getTask('pug-data'));
gulp.task('pug-build' ,getTask('pug-build'));
gulp.task('style-build', getTask('style-build'));
gulp.task('img-min', getTask('img-min'));

gulp.task('default',['pug-build','style-build','watch']);

gulp.task('watch', function () {
    watch(`${paths.pug.data}*.json`, function () {
        gulp.start('pug-data');
    });
    watch(`${paths.pug.tmpl}**/*.pug`, function () {
        gulp.start('pug-build')
    });
    watch([`${paths.pug.pages}*.pug`,`${paths.pug.popup}*.pug`], function () {
        gulp.start('pug-build')
    });
    watch(`${paths.scss}**/*.scss`, function () {
        gulp.start('style-build');
    });
    watch(`${paths.img}**/*`, function () {
        gulp.start('img-min');
    });
});


