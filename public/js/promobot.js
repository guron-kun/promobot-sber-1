
var PromobotConstants = {
    USER_HAS : "user.has.user",
    USER_LOST : "user.lost.user",

    QUESTION_CANCEL : "dialog.question.cancel",
    QUESTION_NEW : "dialog.question.new",
    QUESTION_ANSWER : "dialog.question.answer",

    SAY : "dialog.say",
    USER_SAID : "dialog.user.said",
    ROBOT_REPLIC_START : "dialog.robot.replic.start",
    ROBOT_REPLIC_FINISH : "dialog.robot.replic.finish",
    ABORT_ROBOT_REPLIC : "dialog.robot.replic.abort",
    USER_ACTION : "dialog.action",

    PRINT : "printer.print",
    PRINT_STATE : "printer.state",
    PRINT_STATE_INFO : "printer.state.info",

    PRINT_SUCCESS : "printer.success",

    PRINT_ERROR : "printer.error",
    PRINT_NO_PAPER : "printer.nopaper",
    PRINT_NO_PRINTER : "printer.noprinter",
    STATISTIC : "system.statistic",
    CUSTOM_EVENT_CORE : "system.custom.event.core",
    CUSTOM_EVENT_JS : "system.custom.event.js",
}

class PromobotMessageChannel {
    constructor(lightview){
        this.lightview = lightview;
        var self = this;
        this.lightview.message.connect((message)=>{
            console.debug("accepted message", message);
            if (self.subscriptions[message.type]){
                self.subscriptions[message.type].forEach((subscriber)=>{
                   subscriber.handler(message);
                })
            }
        });

        this.subscriptions = {}
    }



    send(message){
        console.debug("send message", message);
        this.lightview.sendMessage(message);
    }

    subscribe(type, handler){
        var subscribers = this.subscriptions[type];
        
        if (!subscribers){
            var array = []
            this.subscriptions[type] = array;
            subscribers = array;
        }

        subscribers.push({
            handler : handler
        })
    }
}

class PromobotStubMessageChannel extends PromobotMessageChannel {
    constructor(){
        var handlers = [];
        super({
            message : {
                connect : function(handler){
                    handlers.push(handler);
                }
            },

            sendMessage : function(message){
            }
        })
        this.handlers = handlers;
    }

    hasUser(user) {
        var obj = JSON.parse(JSON.stringify(user));
        obj.type = PromobotConstants.USER_HAS;
        this.handler(obj);
    }

    noUser() {
        this.handler({
            type : PromobotConstants.USER_LOST
        })
    }

    customEvent(data){
        var obj = JSON.parse(JSON.stringify(data));
        obj.type = PromobotConstants.CUSTOM_EVENT_CORE;
        this.handler(obj)
    }

    question(text, answers){
        this.handler({
            type : PromobotConstants.QUESTION_NEW,
            text : text,
            answers : answers
        })
    }

    userSaid(text){
        this.handler({
            type : PromobotConstants.USER_SAID,
            text : text
        })
    }

    robotReplicFinish(){
        this.handler({
            type : PromobotConstants.ROBOT_REPLIC_FINISH,
        })
    }

    robotReplicStart(text){
        this.handler({
            type : PromobotConstants.ROBOT_REPLIC_START,
            text : text
        })
    }

    cancelQuestion(){
        this.handler({
            type : PromobotConstants.QUESTION_CANCEL,
        })
    }

    userAction(data){
        this.handler({
            type : PromobotConstants.USER_ACTION,
            meta : data
        })        
    }

    printerStateOk(){
        this.handler({
            type : PromobotConstants.PRINT_STATE_INFO,
            state : "IDLE"
        })
    }

    printerStateError(){
        this.handler({
            type : PromobotConstants.PRINT_STATE_INFO,
            state : "ERROR"
        })
    }

    printResultOk(){
        this.handler({
            type : PromobotConstants.PRINT_SUCCESS
        })
    }

    printResultNoPaper(){
        this.handler({
            type : PromobotConstants.PRINT_NO_PAPER
        })
    }

    printResultNoPrinter(){
        this.handler({
            type : PromobotConstants.PRINT_NO_PRINTER
        })
    }

    handler(message){
        this.handlers.forEach(handler => {
            handler(message);
        })
    }
}


class PromobotService {
    constructor(messageChannel){
        this.messageChannel = messageChannel;
    }

    execute(message, pool){
        var self = this;
        var res, rej;
        
        let promise = new Promise((resolve, reject) => {

            res = resolve;
            rej = reject;

            self.messageChannel.send(message);
        });
        
        pool.push({
            reject : rej,
            resolve : res
        });        

        return promise;
    }
}


class PromobotUserService {
    
    constructor(messageChannel){

        function isNewUser(user, olduser){
            if (!olduser){
                return true;
            }

            return olduser.user_id != user.user_id;
        }

        this.messageChannel = messageChannel;
        
        let messageHandler = (message) => {
            switch (message.type) {
                case PromobotConstants.USER_HAS : {
                    this.hasUser = true;

                    if (this.hasUserCallBack){
                        this.hasUserCallBack(message, isNewUser(message, this.currentUser))
                    }

                    this.currentUser = message;
                    break;
                }
                case PromobotConstants.USER_LOST : {
                    this.hasUser = false;
                    if (this.lostUserCallBack){
                        this.lostUserCallBack()
                    }
                    break;
                }
            }
        }

        this.messageChannel.subscribe(PromobotConstants.USER_HAS, messageHandler);
        this.messageChannel.subscribe(PromobotConstants.USER_LOST, messageHandler);

        this.hasUser = false;
    }

    onUserPresence(callback){
        this.hasUserCallBack = callback;
    }

    onUserLost(callback){
        this.lostUserCallBack = callback;
    }

    isUserPresent(){
        return this.hasUser;
    }

}

class Question{
    constructor(message, messageChannel){
        this.text = message.text;
        this.answers = message.answers;
        this.messageChannel = messageChannel;
    }

    setAnswer(text){
        var response = text;
        
        if (typeof text == "number"){
            if (text < this.answers.length){
                response = this.answers[text];
            } 
        }

        this.messageChannel.send({
            type : PromobotConstants.QUESTION_ANSWER, 
            text : response
        })
    }
}

class PromobotCustomEventService {
    constructor(messageChannel){
        this.messageChannel = messageChannel;
        let messageHandler = (message) => {
            if (this.callback){
                this.callback(message);
            }
        }
        
        this.messageChannel.subscribe(PromobotConstants.CUSTOM_EVENT_CORE, messageHandler);
    }    

    onEvent(callback){
        this.callback = callback;
    }

    send(data){
        var attrs = JSON.parse(JSON.stringify(data))
        attrs.type = PromobotConstants.CUSTOM_EVENT_JS;
        this.messageChannel.send(attrs)
    }
}

class PromobotDialogService {
    
    constructor(messageChannel){
        this.messageChannel = messageChannel;

        let messageHandler = (message) => {
            switch (message.type) {
                case PromobotConstants.QUESTION_NEW : {
                    if (this.question){
                        this.question(new Question(message, messageChannel))
                    }
                    break;
                }
                case PromobotConstants.QUESTION_CANCEL : {
                    if (this.question_cancel){
                        this.question_cancel()
                    }
                    break;
                }
                case PromobotConstants.USER_SAID : {
                    if (this.userSaidCallBack){
                        this.userSaidCallBack(message.text);
                    }
                    break;
                }

                case PromobotConstants.ROBOT_REPLIC_START : {
                    if (this.robotReplicStartCallBack){
                        this.robotReplicStartCallBack(message.text);
                    }
                    break;
                }

                case PromobotConstants.ROBOT_REPLIC_FINISH : {
                    if (this.robotReplicFinishCallBack){
                        this.robotReplicFinishCallBack();
                    }
                    break;
                }


                case PromobotConstants.USER_ACTION : {
                    if (this.actionCallBack){
                        this.actionCallBack(message.meta);
                    }
                    this.hasUser = false;
                    break;
                }

            }
        }

        this.messageChannel.subscribe(PromobotConstants.QUESTION_NEW, messageHandler);
        this.messageChannel.subscribe(PromobotConstants.QUESTION_CANCEL, messageHandler);
        
        this.messageChannel.subscribe(PromobotConstants.USER_ACTION, messageHandler);
        this.messageChannel.subscribe(PromobotConstants.USER_SAID, messageHandler);
        this.messageChannel.subscribe(PromobotConstants.ROBOT_REPLIC_START, messageHandler);
        this.messageChannel.subscribe(PromobotConstants.ROBOT_REPLIC_FINISH, messageHandler);

    }

    onQuestion(newQuestionCallback, cancelQeustionCallback){
        this.question = newQuestionCallback
        this.question_cancel = cancelQeustionCallback
    }

    onAction(callback){
        this.actionCallBack = callback;
    }

    onUserReplic(callback){
        this.userSaidCallBack = callback;
    }

    onRobotReplicStart(callback) {
        this.robotReplicStartCallBack = callback;
    }

    onRobotReplicFinish(callback) {
        this.robotReplicFinishCallBack = callback;
    }

    abortRobotReplic(){
        this.messageChannel.send({
            type : PromobotConstants.ABORT_ROBOT_REPLIC,
        })
    }

    sayReplic(id, macroses){
        this.messageChannel.send({
            type : PromobotConstants.SAY,
            step : id,
            macroses : macroses
        })
    }

    sayText(text){
        this.messageChannel.send({
            type : PromobotConstants.SAY,
            text : text
        })
    }
}


class PromobotPrintSerivce extends PromobotService {
    constructor(messageChannel){
        super(messageChannel);

        var print_result_waiters = [];
        var status_waiters = [];
        
        let resolve = (waiters, data) => {
            waiters.forEach(waiter => {
                waiter.resolve(data)
            });
        }

        let reject = (waiters, error) => {
            waiters.forEach(waiter => {
                waiter.reject(error)
            })
        }
        
        let messageHandler = (message) => {
            switch (message.type) {
                case PromobotConstants.PRINT_SUCCESS : {
                    resolve(print_result_waiters)
                    break;
                }
                case PromobotConstants.PRINT_STATE_INFO : {
                    resolve(status_waiters, message.state);
                    break;
                }
                case PromobotConstants.PRINT_LOW_PAPER: {
                    //ignore for a while
                    break
                }
                case PromobotConstants.PRINT_ERROR : 
                case PromobotConstants.PRINT_NO_PAPER : 
                case PromobotConstants.PRINT_NO_PRINTER : {
                    reject(print_result_waiters, message.type)
                    break;
                }
            }
        }

        this.print_result_waiters = print_result_waiters;
        this.status_waiters = status_waiters;

        this.messageChannel.subscribe(PromobotConstants.PRINT_STATE_INFO, messageHandler);
        this.messageChannel.subscribe(PromobotConstants.PRINT_ERROR, messageHandler);
        this.messageChannel.subscribe(PromobotConstants.PRINT_LOW_PAPER, messageHandler);
        this.messageChannel.subscribe(PromobotConstants.PRINT_NO_PAPER, messageHandler);
        this.messageChannel.subscribe(PromobotConstants.PRINT_NO_PRINTER, messageHandler);
        this.messageChannel.subscribe(PromobotConstants.PRINT_SUCCESS, messageHandler);
    }

    print(data){
        return this.execute({type : PromobotConstants.PRINT, data : data, ignore_not_connected : true}, this.print_result_waiters);
    }

    status(){
        return this.execute({type : PromobotConstants.PRINT_STATE, ignore_not_connected : true}, this.status_waiters);
    }


}

class Promobot {
    constructor(lightview, isStub){

        if (!isStub){
            this.messageChannel = new PromobotMessageChannel(lightview);
        } else {
            this.messageChannel = new PromobotStubMessageChannel();
            this.stub = this.messageChannel;
        }

        this.printService = new PromobotPrintSerivce(this.messageChannel);
        this.dialogService = new PromobotDialogService(this.messageChannel);
        this.userService = new PromobotUserService(this.messageChannel);
        this.custom = new PromobotCustomEventService(this.messageChannel);
    }

    statistic(data, stat_topic){
        this.messageChannel.send({type:"system.statistic", "data" : [data + "\n"], topic: stat_topic});
    }

    static getInstance(){
        return new Promise((resolve, reject) => {
            new QWebChannel(qt.webChannelTransport, (channel) => {
                resolve(new Promobot(channel.objects._promobot_internal, false));
            })
        });
    }

    static getStubInstance(){
        return new Promise((resolve, reject) => {
            setTimeout(function() {
                resolve(new Promobot(null, true));
            }, 10);
            
        });
    }

}
