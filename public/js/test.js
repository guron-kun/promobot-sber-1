//presale
/*
const PHRASES = {
    //переход на главный экран
    "findout-visit-property": {
        action: {
            "goto": "main"
        }
    },
    //Вопрос: карта Сбербанк с собой
    "ask-sbercard": {
        action: {
            "qs": {
                "text": "Скажите, у вас есть карта Сбербанк с собой?",
                "answers": {"consultant": "Да", "wait": "Нет"},
            }
        }
    },
    // переход на экран сервисов самообслуживания
    "consultant": {
        action: {
            "goto": "consultant"
        }
    },
    // кнопка Да -  принята помощь консультанта
    "consultant-accepted": {
        action: {
            "goto": "consultation"
        }
    },
    //кнопка Нет - не принята помощь консультанта
    // переход на экран с вопрсами
    "consultant-rejected": {
        action: {
            "goto": "question"
        }
    },
    // нет карты Сбера - переход на экран ожидания, получения талона
    "wait": {
        action: {
            "goto": "wait"
        }
    },
    // кнопка Спросите меня - переход на экран вопросы-ответы
    "ask-me": {
        action: {
            "goto": "question"
        }
    },
    // кнопка Оцените меня - переход на экран оценки
    "rate-me": {
        action: {
            "goto": "rate"
        }
    },
    // выбрана оценка, переход на последний экран
    "thank": {
        action: {
            "goto": "bye"
        }
    },
    // нажата кнопка Выход, переход на последний экран
    "bye": {
        action: {
            "goto": "bye"
        }
    }
};
*/


//sale
const PHRASES = {
    //переход на главный экран
    "findout-visit-property": {
        action: {
            "goto": "main"
        }
    },
    //Вопрос: карта Сбербанк с собой
    "ask-sbercard": {
        action: {
            "qs": {
                "text": "Скажите, у вас есть карта Сбербанк с собой?",
                "answers": {"consultant": "Да", "wait": "Нет"},
            }
        }
    },
    // переход на экран сервисов самообслуживания
    "consultant": {
        action: {
            "goto": "consultant"
        }
    },
    // кнопка Да -  принята помощь консультанта
    "want-offers": {
        action: {
            "goto": "get-offers"
        }
    },

    "get-tel": {
        action: {
            "goto": "tel"
        }
    },
    "get-offers": {
        action: {
            "goto": "offers"
        }
    },
    "wait-offers": {
        action: {
            "goto": "offers-unavailable"
        }
    },
    "no-offers": {
        action: {
            "goto": "offers-nouser"
        }
    },
    "get-ticket": {
        action: {
            "goto": "print"
        }
    },
    "ticket-unavailible": {
        action: {
            "goto": "print-error"
        }
    },
    // нет карты Сбера - переход на экран ожидания, получения талона
    "wait": {
        action: {
            "goto": "wait"
        }
    },
    // кнопка Спросите меня - переход на экран вопросы-ответы
    "ask-me": {
        action: {
            "goto": "question"
        }
    },
    // кнопка Оцените меня - переход на экран оценки
    "rate-me": {
        action: {
            "goto": "rate"
        }
    },
    // выбрана оценка, переход на последний экран
    "thank": {
        action: {
            "goto": "bye"
        }
    },
    // нажата кнопка Выход, переход на последний экран
    "bye": {
        action: {
            "goto": "bye"
        }
    }
};

const SCREENS_PRESALE = {
    //Э 01
    "main": {
        text: {
            title: 'Выберите или скажите какая операция вас интересует',
        },
        htmlBlock: 'start-menu',
        nav: {
            goStart: false,
            goEnd: false
        }
    },
    //Э 04
    "consultant": {
        text: {
            title: 'Хотите решить вопрос без ожидания очереди?',
            plh1: 'Уважаемый клиент, вы  можете не ждать очереди и выполнить операцию в Личном кабинете. \n' + 'Наш консультант поможет вам в этом',
            plh2: 'Хотите продолжить обслуживание с консультантом?'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Да',
                action: 'consultant-accepted',
            },
            {
                text: 'Нет',
                action: 'consultant-rejected',
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 05
    "consultation": {
        text: {
            title: 'Пожалуйста, подойдите к консультанту',
            plh1: 'Пожалуйста, обратитесь к консультанту в зале и он поможет вам оперативно решить вопрос.',
            plh2: ''
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 05_2
    "wait": {
        text: {
            title: 'Пожалуйста, ожидайте очереди',
            plh1: 'Если вы еще не взяли талон, то можете получить его в терминале на входе в отделение.',
            plh2: ''
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 09
    "question": {
        text: {
            title: 'Задайте мне вопросы о Сбербанке',
            plh1: 'Могу я быть еще чем-то полезен?',
            plh2: 'Спросите меня. Я готов ответить на вопросы о Сбербанке.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 10
    "rate": {
        text: {
            title: 'Пожалуйста, оцените мою работу',
        },
        htmlBlock: 'rate',
        nav: {
            goStart: false,
            goEnd: true
        }
    },
    //Э 11
    "bye": {
        text: {
            title: 'Всего доброго',
        },
        htmlBlock: 'bye-screen',
        nav: {
            goStart: true,
            goEnd: false
        }
    },
};

const SCREENS_SALE = {
    //Э 01
    "main": {
        text: {
            title: 'Выберите или скажите какая операция вас интересует',
        },
        htmlBlock: 'start-menu',
        nav: {
            goStart: false,
            goEnd: false
        }
    },
    //Э 04
    "consultant": {
        text: {
            title: 'Хотите решить вопрос без ожидания очереди?',
            plh1: 'Уважаемый клиент, вы  можете не ждать очереди и выполнить операцию в Личном кабинете. \n' + 'Наш консультант поможет вам в этом',
            plh2: 'Хотите продолжить обслуживание с консультантом?'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Да',
                action: 'want-offers',
            },
            {
                text: 'Нет',
                action: 'wait',
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 05_1
    "get-offers": {
        text: {
            title: 'Узнайте о персональных предложениях',
            plh1: 'Пожалуйста, обратитесь к консультанту в зале и он поможет вам.',
            plh2: 'А пока хотите узнать о доступных для вас спецпредложениях?'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Cпецпредложения',
                action: 'get-tel'
            },
            {
                text: 'Спасибо, не надо',
                classes: 'btn_nope',
                action: 'bye'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 05_2
    "wait": {
        text: {
            title: 'Пожалуйста, ожидайте очереди',
            plh1: 'Если вы еще не взяли талон, то можете получить его в терминале на входе в отделение.',
            plh2: ''
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 06
    "tel": {
        text: {
            title: 'Узнайте о персональных предложениях',
            plh1: '',
            plh2: ''
        },
        htmlBlock: 'get-tel',
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 07
    "offers": {
        text: {
            title: 'Выберите интересующие персональные предложения',
            plh1: '',
            plh2: ''
        },
        htmlBlock: 'offers-screen',
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 08
    "print": {
        text: {
            title: 'Пожалуйста, возьмите талон и подойдите к консультанту',
            plh1: '',
            plh2: ''
        },
        htmlBlock: 'print-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 08_1
    "print-error": {
        text: {
            title: 'Пожалуйста, подойдите к консультанту',
            plh1: 'Извините, не могу напечатать вам талон.',
            plh2: 'Пожалуйста, подойдите к консультанту для оформления персональных предложений и решения вашего вопроса.'
        },
        htmlBlock: 'action-screen',
        nav: {
            goStart: false,
            goEnd: true
        }
    },
    //Э 08_2
    "offers-nouser": {
        text: {
            title: 'Пожалуйста, подойдите к консультанту',
            plh1: 'К сожалению, мне недоступна информация о доступных вам предложениях.',
            plh2: 'Пожалуйста, подойдите к консультанту для решения вопроса, с которым вы пришли.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 08_3
    "offers-unavailable": {
        text: {
            title: 'Пожалуйста, подойдите к консультанту',
            plh1: 'На текущий момент, предложения для вас подготавливаются.',
            plh2: 'Пожалуйста, подойдите к консультанту для оформления персональных предложений и решения вашего вопроса.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 09
    "question": {
        text: {
            title: 'Задайте мне вопросы о Сбербанке',
            plh1: 'Могу я быть еще чем-то полезен?',
            plh2: 'Спросите меня. Я готов ответить на вопросы о Сбербанке.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 10
    "rate": {
        text: {
            title: 'Пожалуйста, оцените мою работу',
        },
        htmlBlock: 'rate',
        nav: {
            goStart: false,
            goEnd: true
        }
    },
    //Э 11
    "bye": {
        text: {
            title: 'Всего доброго',
        },
        htmlBlock: 'bye-screen',
        nav: {
            goStart: true,
            goEnd: false
        }
    },
};

let branch = 'sale';
let SCREENS = SCREENS_PRESALE;

if (branch === 'sale') {
    SCREENS = SCREENS_SALE;
}


let myApp = {
    screens: document.querySelectorAll('.content-wrap'),
    actionPlh: {
        title: document.querySelector('#screen-title'),
        plh1: document.querySelector('#plh1'),
        plh2: document.querySelector('#plh2')
    },
    modal: {
        overlay: document.querySelector('#overlay'),
        window: document.querySelector('#modal'),
        txt: document.querySelector('#modal-txt'),
        btns: document.querySelector('#modal-btns')
    },
    navBtns: {
        goStart: document.querySelector('#go-start'),
        goEnd: document.querySelector('#go-end')
    },
    subtitles: {
        el: document.querySelector('#subtitles'),
        longTxtClass: 'is-long-txt'
    },
    timeout: {
        clearSub: null
    },
    curScreen: 'main',
    userService: '',
    muteBtn: document.querySelector('#btn-mute'),
    rates: ['very good', 'good', 'normal', 'bad', 'very bad'],
    statistic: {
        user: null,
        rate: null
    },

    handleUserAction: function (action, obj = SCREENS, scActions = screenActions) {
        console.log('handleUserAction', action);

        if (myApp.hasOwnProperty('ModalDialog')) {
            myApp.ModalDialog.closeDialog();
        }

        myApp.clearSub();

        let data = obj[action];

        if (!!data) {
            //Если есть каке-то действия привязанные к экрану
            if (scActions.hasOwnProperty(action)) {
                scActions[action](myApp);
            }

            let currentBlock = changeBlock(data.htmlBlock, myApp.screens);

            if (data.hasOwnProperty('text')) {
                changeText(data.text, myApp.actionPlh);
            }

            if (data.hasOwnProperty('nav')) {
                for (let btn in data.nav) {
                    if (data.nav[btn] === true) {
                        myApp.navBtns[btn].removeAttribute('hidden');
                    } else {
                        myApp.navBtns[btn].setAttribute('hidden', 'hidden');
                    }
                }
            }

            if (data.hasOwnProperty('btns')) {
                let btnBar = currentBlock.querySelector('.button-bar');
                let nb;

                removeSiblings(btnBar);

                for (let i = 0; i < data.btns.length; i++) {
                    nb = document.createElement('button');
                    nb.classList.add('btn');
                    nb.classList.add('js-action');
                    nb.textContent = data.btns[i].text;
                    nb.dataset.action = data.btns[i].action;
                    if (data.btns[i].classes) {
                        nb.classList.add(data.btns[i].classes);
                    }
                    btnBar.appendChild(nb);

                    nb.addEventListener('click', function (e) {
                        handleStep(this.dataset.action);
                        e.preventDefault();
                    });
                }
            }
        }

        function changeBlock(blockId = 'start-menu', screens) {

            screens.forEach((item) => {
                item.classList.add('is-hidden');
            });

            let currentBlock = document.getElementById(blockId);

            currentBlock.classList.remove('is-hidden');

            return currentBlock;
        }

        function changeText(txts, elems) {
            for (let txt in txts) {
                if (txts.hasOwnProperty(txt)) {
                    elems[txt].textContent = txts[txt];
                }
            }
        }
    },

    clearSub: function () {
        myApp.subtitles.el.classList.remove(myApp.subtitles.longTxtClass);
        myApp.subtitles.el.textContent = '';
    },

    showSub: function (txt) {
        myApp.clearSub();

        let k = findSubStringsRatio(txt, myApp.subtitles.charW);

        if (k >= 2) {
            myApp.subtitles.el.classList.add(myApp.subtitles.longTxtClass);
        }

        if (k >= 6) {
            txt = kitcut(txt, 430);
        }

        myApp.subtitles.el.textContent = txt;

    },

    muteRobot: function (muted = true) {
        if (this.muteBtn) {
            this.muteBtn.addEventListener('click', function () {
                robot.dialogService.abortRobotReplic();
                this.classList.add('ico-mic-muted');
            })
        }
    }
};

const TIMEOUT = {
    setRate: {
        fn: function () {
            console.log('timeout set rate');

            //это реплика связанная с action - переход на последний экран
            robot.dialogService.sayReplic('bye');

            myApp.timeout.setRate = null;

            //on test
            robot.stub.customEvent({stopCase: true});
            robot.stub.userAction('bye');
            //on test
        },
        t: 1000
    },
    clearSub: {
        fn: myApp.clearSub(),
        t: 5000
    },
    printRateMe: {
        fn: function () {
            robot.dialogService.sayReplic('rate-me');

            myApp.timeout.printRateMe = null;

            //on test
            handleStep('rate-me');
            //on test
        },
        t: 4000
    },
    gotoMain: {
        fn: function () {
            //robot.dialogService.sayReplic('findout-visit-property');
            robot.dialogService.sayReplic('back-to-main');

            myApp.timeout.gotoMain = null;

            //on test
            robot.stub.customEvent({stopCase: true});
            robot.stub.customEvent({startCase: true});
            robot.stub.userAction(['screen', 'main']);
            //on test
        },
        t: 4000
    },
    userIdle: {
        fn: function (replic) {
            robot.dialogService.sayReplic(replic);

            //on test
            console.log('userIdle: ', replic);
            robot.stub.robotReplicStart(replic);
            //on test

            myApp.timeout.userIdle = setTimeout(TIMEOUT.userIdle.fn, TIMEOUT.userIdle.t, replic);
        },
        t: 20000
    }
};

//Действия привязаныне к экрану. Вызывается если приходит userAction типа screen&main
//и обаратывается в handleUserAction
let screenActions = {
    tel: function () {
        myApp.timeout.userIdle = setTimeout(TIMEOUT.userIdle.fn, TIMEOUT.userIdle.t, 'tel-timer');
    },
    offers: function () {
        myApp.timeout.userIdle = setTimeout(TIMEOUT.userIdle.fn, TIMEOUT.userIdle.t, 'choose-offer');
    },
    print: function () {
        myApp.timeout.printRateMe = setTimeout(TIMEOUT.printRateMe.fn, TIMEOUT.printRateMe.t);
    },
    bye: function () {
        myApp.timeout.gotoMain = setTimeout(TIMEOUT.gotoMain.fn, TIMEOUT.gotoMain.t);
        robot.custom.send({"caseFinished": true});
    }
};

function ShowDialog(appData) {
    let fn = this;
    const CLASSES = {'nope': 'btn_nope', 'wide': 'modal-btn_wide'};

    fn.openDialog = function (text) {
        appData.modal.overlay.classList.add('is-open');
        appData.modal.window.classList.add('is-open');
        robot.dialogService.sayReplic(text);
    };

    fn.closeDialog = function () {
        appData.modal.overlay.classList.remove('is-open');
        appData.modal.window.classList.remove('is-open');
        appData.modal.txt.textContent = '';

        removeSiblings(appData.modal.btns);
    };

    fn.createWindow = function (modalData, handler) {
        appData.modal.txt.textContent = modalData.text;

        if (modalData.answers) {
            removeSiblings(appData.modal.btns);

            let wideBtn = modalData.answers.length > 2;

            for (let val in modalData.answers) {

                let btn = document.importNode(document.querySelector('#tmpl-btn').content, true).querySelector('button');

                if (modalData.answers[val] === 'Нет') {
                    btn.classList.add(CLASSES.nope);
                }

                if (wideBtn) {
                    btn.classList.add(CLASSES.wide);
                }

                btn.textContent = modalData.answers[val];
                appData.modal.btns.appendChild(btn);

                if (handler) {
                    btn.onclick = event => {
                        handler(btn.textContent);
                        robot.custom.send({"userClick": true});
                    }
                }
                //вызвано кликом по кнопке
                else {
                    btn.dataset.action = val;
                    btn.addEventListener('click', function (e) {
                        handleStep(this.dataset.action);
                        e.preventDefault();
                    });
                }
            }

            fn.openDialog(modalData.text);
        }
    };
}

//реплики-шаги и действия с ними связанные
//action - реплика
function handleStep(step, replics = PHRASES, screens = SCREENS) {
    console.log('handleStep', step);

    let act = replics[step];

    if (act !== undefined) {
        myApp.ModalDialog.closeDialog();
        myApp.clearSub();
        //тип действия повешенного на реплику
        //пока рассматривает одно действие на реплику
        //goto - переход к экрану
        //qs - задать вопрос, т.е. собрать и открыть попап

        switch (Object.keys(act.action)[0]) {

            //action переход на экран
            case 'goto':
                myApp.handleUserAction(act.action.goto, screens);
                break;
            //action - открыть попап-диалог
            case 'qs':

                //надо бы передать действия для кнопок в попапе, но через api похоже никак
                //robot.stub.question(act.action.qs.text, act.action.qs.answers, act.action.qs.say);

                myApp.ModalDialog.createWindow({
                    "text": act.action.qs.text,
                    "answers": act.action.qs.answers
                });
                break;
        }

    } else {
        console.log(action + ' Действие не описано');
    }
}

//произнести реплику и вызвать обработчик по клику на элемента
//опять не понял - нужен ли тут обработчик
//или только реплика и ждать onAction
function setTouchEvents(selector = '.js-action', replics = PHRASES) {
    let elems = document.querySelectorAll(selector);

    for (let el of elems) {
        el.addEventListener('click', function () {
            let act = this.dataset.action;

            if (act) {
                robot.dialogService.sayReplic(act);

                robot.custom.send({"userClick": true});

                //test
                handleStep(act);
            }
        })
    }
}

function addStartStopNavEvents() {
    const beginBtn = document.querySelector('#go-start');
    const finishBtn = document.querySelector('#go-end');

    beginBtn.addEventListener('click', function () {
        //on test
        robot.stub.customEvent({'startCase': true});
        handleStep('findout-visit-property');
        //on test

        robot.dialogService.sayReplic('findout-visit-property');
        robot.custom.send({"caseFinished": true});
        robot.custom.send({"caseStarted": true});

    });

    finishBtn.addEventListener('click', function () {
        //on test
        robot.stub.customEvent({'stopCase': true});
        handleStep('bye');
        //on test

        robot.dialogService.sayReplic('bye');
    })
}

function Rating() {
    let fn = this;

    let rate = document.querySelector('#rate');

    let btns = rate.querySelectorAll('.rate-item');

    fn.setRate = function () {
        for (let btn of btns) {
            btn.addEventListener('click', function () {
                if (fn.setSelected(this)) {
                    robot.custom.send({"userClick": true});

                    //это реплика-оценка
                    robot.dialogService.sayReplic(btn.dataset.say);
                    myApp.statistic.rate = btn.dataset.say;

                    //запись статистики
                    writeStatistic(myApp.statistic);

                    myApp.timeout.setRate = setTimeout(TIMEOUT.setRate.fn, TIMEOUT.setRate.t);
                }
            })
        }
    };

    fn.clearRate = function () {
        for (let btn of btns) {
            btn.classList.remove('is-selected');
        }
    };

    fn.setSelected = function (btn) {
        let selected = rate.querySelectorAll('.is-selected');

        if (selected.length) {
            return false;
        }

        btn.classList.add('is-selected');

        return true;
    };
}

function handleRate(rate) {
    console.log('handleRate', rate);

    let rateBtn = document.querySelector('[data-say=' + rate + ']');

    rateBtn.click();
}

function writeStatistic(data) {
    robot.statistic(
        formatDate(new Date) + ";"
        + data.user + ";"
        + data.rate, "sale"
    )
}

document.addEventListener('DOMContentLoaded', function () {
    //загрузка шаблона талона
    loadTalonTemplate();

    //запуск счетчика талонов
    myApp.ticketCounter = new TicketCounter;

    myApp.screens = document.querySelectorAll('.content-wrap');

    myApp.actionPlh = {
        title: document.querySelector('#screen-title'),
        plh1: document.querySelector('#plh1'),
        plh2: document.querySelector('#plh2')
    };

    myApp.subtitles.charW = findCharWidth('Е');

    //запись какую услугу выбрали на главном экране
    let userService = document.querySelectorAll('.js-service');

    for (let el of userService) {
        el.addEventListener('click', function () {
            myApp.userService = el.querySelector('.card__title').textContent;
        })
    }

    //add touch events hendlers
    setTouchEvents();
    addStartStopNavEvents();

    let RobotRating = new Rating;

    RobotRating.setRate();

    myApp.ModalDialog = new ShowDialog(myApp);

    //== начало взаимодействия

    Promobot.getStubInstance().then((promobot) => {

        robot = promobot;

        promobot.userService.onUserPresence((user, isNew) => {
            console.log("is new", isNew, "user : ", user);
            myApp.statistic.user = user.user_id;
        });

        promobot.userService.onUserLost(no => {
            console.log("userLost");
        });

        promobot.dialogService.onUserReplic(text => {
            console.log("user said " + text);

            clearTimeout(myApp.timeout.clearSub);

            myApp.showSub(clearSubString(text));

            myApp.timeout.clearSub = setTimeout(TIMEOUT.clearSub.fn, TIMEOUT.clearSub.t);

            switch (myApp.curScreen) {
                case 'print':
                    stopTimeout('printRateMe', 'pause');
                    break;
                case 'bye':
                    stopTimeout('gotoMain', 'pause');
                    break;
            }

            //таймеры бездействия пользователя
            if (myApp.timeout.userIdle !== null) {
                stopTimeout('userIdle', 'pause');
            }
        });

        promobot.custom.onEvent(data => {
            console.log("custom event", data);

            //case start
            if (data.startCase === true) {
                robot.dialogService.sayReplic('findout-visit-property');

                //очистка поля телефон
                if (myApp.hasOwnProperty('tel')) {
                    myApp.tel.value = '';
                }

                //удаление карточек выбранных персональных предложений
                if (myApp.hasOwnProperty('offersCards')) {
                    myApp.offersCards.removeAll();
                }

                //очистка слайдов персональных предложений
                if (myApp.hasOwnProperty('offersSlider')) {
                    myApp.offersSlider.removeSlides();
                }

                //очистка субтитров
                myApp.clearSub();

                //очистка рейтинга
                RobotRating.clearRate();

                myApp.statistic.user = null;
                myApp.statistic.rate = null;
            }

            //case finish
            if (data.stopCase === true) {
                robot.dialogService.sayReplic('bye');
            }
        });

        promobot.dialogService.onRobotReplicStart(text => {
            console.log("robot start replic " + text);

            myApp.showSub(clearSubString(text));

            clearTimeout(myApp.timeout.clearSub);

            switch (myApp.curScreen) {
                case 'print':
                    stopTimeout('printRateMe', 'pause');
                    break;
                case 'bye':
                    stopTimeout('gotoMain', 'pause');
                    break;
            }

            //таймеры бездействия пользователя
            if (myApp.timeout.userIdle !== null) {
                stopTimeout('userIdle', 'pause');
            }
        });

        promobot.dialogService.onRobotReplicFinish(nothing => {
            console.log("robot finish replic");
            myApp.clearSub();
            myApp.muteBtn.classList.remove('ico-mic-muted');

            clearTimeout(myApp.timeout.clearSub);

            switch (myApp.curScreen) {
                case 'print':
                    if (myApp.timeout.printRateMe === 'pause') {
                        setTimeout(TIMEOUT.printRateMe.fn, TIMEOUT.printRateMe.t);
                    }
                    break;
                case 'bye':
                    if (myApp.timeout.gotoMain === 'pause') {
                        setTimeout(TIMEOUT.gotoMain.fn, TIMEOUT.gotoMain.t);
                    }
                    break;
            }

            //таймеры бездействия пользователя
            if (myApp.timeout.userIdle === 'pause') {
                setTimeout(TIMEOUT.userIdle.fn, TIMEOUT.userIdle.t);
            }
        });

        promobot.dialogService.onQuestion(question => {
                myApp.ModalDialog.createWindow(question, answer => {
                    question.setAnswer(answer);
                    myApp.ModalDialog.closeDialog();
                });
            },
            nothing => {
                myApp.ModalDialog.closeDialog();
            }
        );

        promobot.dialogService.onAction(action => {
            console.log("onAction", action);

            //при переходе

            if (Array.isArray(action)) {
                for (let i = 0; i < action.length; i++) {
                    if (action[i] === 'call-step-phrase') {
                        let phrase = action[i + 1];

                        console.log('onAction phrase', phrase);

                        //on test
                        handleStep(phrase);
                        //on test

                        delete action[i];
                        delete action[i + 1];
                    }

                    if (action[i] === 'screen') {
                        let screenName = action[i + 1];
                        console.log('onAction screen: ', screenName);

                        myApp.curScreen = screenName;

                        //при переходе на другой экран очищаются все запученные таймеры
                        for (let timer in myApp.timeout) {
                            //if (timer !== null) {
                                stopTimeout(timer);
                            //}
                        }

                        //и закрыть попап персональных предложений
                        myApp.offersPopup.close();

                        //открыть попап голосом
                        if(screenName === 'offers-popup'){
                            let slide = parseInt(action[i + 2]);

                            myApp.offersSlider.gotoSlide(slide - 1);
                            myApp.offersPopup.open();

                            delete action[i];
                            delete action[i + 1];
                            delete action[i + 2];
                        }else{
                            myApp.handleUserAction(screenName);

                            delete action[i];
                            delete action[i + 1];
                        }
                    }

                    if (action[i] === 'rate') {
                        console.log('onAction rate', action[i + 1]);
                        handleRate(action[i + 1]);

                        delete action[i];
                        delete action[i + 1];
                    }


                    if (action[i] !== undefined) {
                        console.log('onAction default', action[i]);
                        myApp.handleUserAction(action[i + 1]);
                    }
                }
            } else {
                myApp.handleUserAction(action);
            }
        })
    });

    myApp.muteRobot();

    //для sale
    if (branch === 'sale') {
        let telInput = document.querySelector('#tel');
        telInput.addEventListener('input', mask, false);
        telInput.addEventListener('focus', mask, false);
        telInput.addEventListener('blur', mask, false);

        myApp.tel = telInput;

        validateTelInput(telInput);

        myApp.offersPopup = new OffersPopup(myApp);
    }
});

function formatDate(date) {
    let day = date.getDate() + "";
    let month = (date.getMonth() + 1) + "";
    let year = date.getFullYear() + "";


    if (day.length < 2) {
        day = 0 + day;
    }

    if (month.length < 2) {
        month = 0 + month;
    }

    return day + "." + month + "." + year + " " + ('0' + date.getHours()).slice(-2) + ":" + ('0' + date.getMinutes()).slice(-2) + ":" + ('0' + date.getSeconds()).slice(-2);

    // const dateFormatOptions = {
    //     year: 'numeric',
    //     month: '2-digit',
    //     day: '2-digit',
    //
    //     hour: '2-digit',
    //     minute: '2-digit',
    //     second: '2-digit'
    // };

    //return date.toLocaleString("ru", dateFormatOptions);
}

function removeSiblings(parent) {
    while (parent.firstElementChild) {
        parent.firstElementChild.remove();
    }
}

function clearSubString(str) {
    if (str.length) {
        let clearStr = str.replace(/{[^}]+}/g, '');
        clearStr = clearStr.replace('+', '');
        clearStr = clearStr.replace(/ {1,}/g, " ");

        return clearStr;
    }
}

function findCharWidth(str) {
    let test = document.createElement('div');
    test.classList.add('char-test');
    test.textContent = str;
    document.body.appendChild(test);
    let w = test.clientWidth;
    test.remove();
    return w;
}

function findSubStringsRatio(str, charW, maxpx = 1000) {

    let strW = str.length * charW;

    return strW / maxpx;
}

function kitcut(text, limit) {

    if (text.length <= limit) return text;

    text = text.slice(0, limit);

    let lastSpace = text.lastIndexOf(" ");

    if (lastSpace > 0) {
        text = text.substr(0, lastSpace);
    }

    return text + "...";
}

//sale
function validateTelInput(field) {
    const ERRORS = ['Поле обязательно для заполнения', 'Пожалуйста, введите телефон правильно'];
    let telBlock = document.querySelector('#get-tel');
    let btn = telBlock.querySelector('#btn-get-tel');
    let formCol = telBlock.querySelector('.form-col');
    let colError = formCol.querySelector('.error-desc');

    field.addEventListener('focus', function () {
        clearColError(formCol, colError);
    });

    field.addEventListener('keyup', function () {
        let check = checkField(field, false);

        if (check) {
            //остановка таймера ожидания заполнения поля
            //запуск таймера ожидания клика по кнопке
            stopTimeout('userIdle');
            myApp.timeout.userIdle = setTimeout(TIMEOUT.userIdle.fn, TIMEOUT.userIdle.t, 'click-offers');
        } else if (!check && this.value.length === 0) {
            //остановка таймера ожидания клика по кнопке
            //запуск таймера ожидания заполнения поля
            stopTimeout('userIdle');
            myApp.timeout.userIdle = setTimeout(TIMEOUT.userIdle.fn, TIMEOUT.userIdle.t, 'tel-timer');
        }
    });

    btn.addEventListener('click', function () {
        robot.custom.send({"userClick": true});

        clearColError(formCol, colError);

        if (checkField(field)) {
            let telno = '7' + field.value.trim();
            telno = telno.replace(' ', '');
            telno = telno.replace('(', '');
            telno = telno.replace(')', '');
            telno = telno.replace(/-/g, '');

            myApp.offersCards = new CreateOfferCard(telno);
            stopTimeout('userIdle');

            //on test
            handleStep('get-offers');
            //on test
        }
    });

    function checkField(field, showError = true) {
        let check = true;

        if (field.value.length === 0) {
            if (showError) {
                showColError(formCol, colError, ERRORS[0]);
            }

            //robot.dialogService.sayReplic('tel-error');
            check = false;
        }

        //18 - это вместе с пробелами, скобками и прочим мусором
        if (field.value.length > 0 && field.value.length < 15) {
            if (showError) {
                showColError(formCol, colError, ERRORS[1]);
            }

            robot.dialogService.sayReplic('click-offers-error');
            check = false;
        }

        return check;
    }

    function showColError(col, error, errorDesc) {
        error.textContent = errorDesc;
        col.classList.add('col-error');
    }

    function clearColError(col, error) {
        error.textContent = '';
        col.classList.remove('col-error');
    }
}

function setCursorPosition(pos, elem) {
    elem.focus();
    if (elem.setSelectionRange) {
        elem.setSelectionRange(pos, pos);
    } else if (elem.createTextRange) {
        let range = elem.createTextRange();
        range.collapse(true);
        range.moveEnd("character", pos);
        range.moveStart("character", pos);
        range.select()
    }
}

//+7 (___) __-___-__
//+7 (___) ___ ____
function mask(event) {
    let matrix = "(___) __-___-__";
    let i = 0;
    let def = matrix.replace(/\D/g, "");
    let val = this.value.replace(/\D/g, "");

    if (def.length >= val.length) {
        val = def;
    }

    this.value = matrix.replace(/./g, function (a) {
        return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
    });

    if (event.type === "blur") {
        if (this.value.length === 2) {
            this.value = "";
        }
    } else {
        setCursorPosition(this.value.length, this);
    }
}

function CheckPersonalOffer() {
    let fn = this;

    let block = document.querySelector('#offers-screen');

    let check = block.querySelectorAll('.js-check-offer input[type=checkbox]');

    let btn = block.querySelector('#btn-print-offer');

    myApp.checkedOffers = {};

    function setCheckState(el) {
        let lbl = el.parentElement;

        if (el.checked) {
            lbl.classList.add('is-checked');
            myApp.checkedOffers[el.name] = true;

            //TODO: заменить эту хрень на что-нибудь нормальное, например выбор по индексу
            document.querySelector(`[data-offer="${el.name}"]`).classList.add('is-selected');
        } else {
            lbl.classList.remove('is-checked');
            delete myApp.checkedOffers[el.name];
            document.querySelector(`[data-offer="${el.name}"]`).classList.remove('is-selected');
        }
    }

    for (let item of check) {
        item.addEventListener('change', function () {

            robot.custom.send({"userClick": true});

            setCheckState(item);

            if (!Object.keys(myApp.checkedOffers).length) {
                //проверка - открыт попап предложений или нет
                //TODO: переделать проверку
                if (document.querySelector('#offers-popup').classList.contains('is-open')) {
                    myApp.timeout.userIdle = setTimeout(TIMEOUT.userIdle.fn, TIMEOUT.userIdle.t, 'choose-timer');
                } else {
                    myApp.timeout.userIdle = setTimeout(TIMEOUT.userIdle.fn, TIMEOUT.userIdle.t, 'choose-offer');
                }
            } else {
                stopTimeout('userIdle');
            }
        });
    }

    fn.check = function (index) {
        let item = check[index];
        item.click();
        return item.checked;
    };

    // fn.uncheckAll = function () {
    //     for(let item of check){
    //         item.checked = false;
    //         setCheckState(item);
    //     }
    // };

    btn.addEventListener('click', function () {
        //ticket-error - нет выбранных
        //get-ticket - все ок
        //ticket-unavailible - ошибка печати
        robot.custom.send({"userClick": true});

        if (!Object.keys(myApp.checkedOffers).length) {
            robot.dialogService.sayReplic('ticket-error');
        } else {
            //проверка принтера
            //robot.dialogService.sayReplic('get-ticket');

            letPrintTicket();

            //on test
            handleStep('get-ticket');
            //on test
        }

    });
}

function CreateOfferCard(tel) {
    let offersMenu = document.querySelector('#offers-menu');
    let offersSlider = document.querySelector('#offers-slider .slider__content');
    const IMGPATH = 'offers-pics/';
    let offers = new PersonalOffers("http://localhost/pp");

    offers.get(tel, 3).then(data => {
        // get-offers - юзер есть в базе, есть предложения
        // wait-offers - нет предложений,
        // no-offers - юзер не найден

        if (data.offers.length) {

            let count = -1;

            for (let item of data.offers) {

                count++;

                let card = document
                    .importNode(document.querySelector('#tmpl-offer-card').content, true)
                    .querySelector('.offer-card');

                card.querySelector('.card__title').textContent = item.title;
                card.querySelector('.offer-card__desc').textContent = item.description;
                card.querySelectorAll('input[type=checkbox]')[0].name = item.title;


                let openPopup = card.querySelector('.offer-card__more');
                openPopup.dataset.index = count;

                openPopup.addEventListener('click', function (e) {
                    myApp.offersSlider.gotoSlide(parseInt(this.dataset.index));
                    myApp.offersPopup.open();

                    //очистка таймаута ожидания экрана ПП
                    stopTimeout('userIdle');

                    //и запуск таймаута ожидания экрана ПП с открытым попапом
                    myApp.timeout.userIdle = setTimeout(TIMEOUT.userIdle.fn, TIMEOUT.userIdle.t, 'choose-timer');

                    //заткнуть
                    robot.dialogService.abortRobotReplic();

                    e.preventDefault();
                });

                let slide = document
                    .importNode(document.querySelector('#tmpl-offer-slide').content, true)
                    .querySelector('.offer-slide');

                let imgName = item.picture;

                if (imgName < 10) {
                    imgName = '0' + imgName;
                }

                slide.querySelector('.offer-slide__title').textContent = item.title;
                slide.querySelector('img').src = `${IMGPATH}${imgName}.png`;
                slide.dataset.say = item.replic;

                let btnInterest = slide.querySelector('.btn');
                btnInterest.dataset.offer = item.title;

                btnInterest.addEventListener('click', function (e) {
                    robot.custom.send({"userClick": true});

                    let check = myApp.checkOffer.check(myApp.offersSlider.getCurIndex(offersSlider));

                    if (check) {
                        robot.dialogService.sayReplic('offer-checked');

                        //on test
                        robot.stub.robotReplicStart('offer-checked')
                        //on test
                    }

                    e.preventDefault();
                });

                offersMenu.appendChild(card);
                offersSlider.appendChild(slide);
            }

            robot.dialogService.sayReplic('get-offers');

            myApp.checkOffer = new CheckPersonalOffer;
            myApp.offersSlider = new OffersSlider;
        } else {
            robot.dialogService.sayReplic('wait-offers');
        }
    });

    this.removeAll = function () {
        removeSiblings(offersMenu);
    }
}

function OffersSlider() {
    let fn = this;
    let slider = document.querySelector('#offers-slider');
    let sliderContent = slider.querySelector('.slider__content');
    let navLeft = slider.querySelector('.nav-left');
    let navRight = slider.querySelector('.nav-right');
    let slides = slider.querySelectorAll('.slide');
    let slidesNum = slides.length;

    if (slidesNum < 2) {
        navLeft.setAttribute('hidden', '');
        navRight.setAttribute('hidden', '');
    }

    checkNav(navLeft, navRight, 0);

    function checkNav(left, right, index) {
        if (index === 0) {
            left.classList.add('is-disabled');
            right.classList.remove('is-disabled');
        } else if (index > 1 && index >= slidesNum - 1) {
            left.classList.remove('is-disabled');
            right.classList.add('is-disabled');
        } else {
            left.classList.remove('is-disabled');
            right.classList.remove('is-disabled');
        }
    }

    //TODO: выпилить это угребище
    fn.getCurIndex = function (slider) {
        let res = 0;

        let str = slider.style.transform;

        if (!str.length) {
            return res;
        }

        let n = str.indexOf("(");
        let n1 = str.indexOf(")");

        res = parseInt(str.slice(n + 1, n1 - 1));

        return Math.abs(res / 100);
    };

    fn.gotoSlide = function (index) {
        sliderContent.style.transform = 'translateX(-' + (100 * index) + '%)';

        fn.getCurIndex(sliderContent);

        checkNav(navLeft, navRight, index);

        //заткнуть
        robot.dialogService.abortRobotReplic();

        //сказать
        robot.dialogService.sayText(sliderContent.children[index].dataset.say);
        console.log(sliderContent.children[index].dataset.say);
    };

    fn.removeSlides = function () {
        removeSiblings(sliderContent);
    };

    navLeft.addEventListener('click', function () {
        let index = fn.getCurIndex(sliderContent);

        checkNav(navLeft, navRight, index);
        fn.gotoSlide(index - 1);
    });

    navRight.addEventListener('click', function () {
        let index = fn.getCurIndex(sliderContent);

        checkNav(navLeft, navRight, index);
        fn.gotoSlide(index + 1);
    });

}

function OffersPopup(mainObj) {
    let fn = this;

    let popup = document.querySelector('#offers-popup');
    let close = popup.querySelector('.popup-close');

    fn.open = function () {
        popup.classList.add('is-open');
        mainObj.modal.overlay.classList.add('is-open', 'is-dark');

        //заткнуть
        robot.dialogService.abortRobotReplic();
    };

    fn.close = function () {
        popup.classList.remove('is-open');
        mainObj.modal.overlay.classList.remove('is-open', 'is-dark');

        //заткнуть
        robot.dialogService.abortRobotReplic();
    };

    close.addEventListener('click', function () {
        fn.close();

        if (!Object.keys(mainObj.checkedOffers).length) {
            robot.dialogService.sayReplic('offers-empty');

            //on test
            robot.stub.robotReplicStart('offers-empty')
            //on test
        } else {
            robot.dialogService.sayReplic('offers-saved');

            //on test
            robot.stub.robotReplicStart('offers-saved')
            //on test
        }
    });
}

function stopTimeout(timeout, state = null) {
    console.log('stop timeout: ', timeout);

    clearTimeout(myApp.timeout[timeout]);
    myApp.timeout[timeout] = state;
}

//----talon----

//обработка ошибок печати
function handlePrinterError(error) {
    let message = "test promobot";

    switch (error) {
        case ("IDLE") : {
            return false;
        }
        case ("IN_PROCESS") : {
            //showError("Принтер занят. Пожалуйста, повторите операцию через несколько секунд.");
            return true;
        }
        case ("LOW_PAPER") : {
            message = settings.message_printer_paper_near_end.split("{kiosk_title}").join(host);
            http.PRINTER_ERROR({text: message});
            return false;
        }
        case ("NOT_CONNECTED") : {
            message = "Не подключен принтер";
            break;
        }
        case ("NO_PAPER") : {
            message = settings.message_kiosk_no_paper;
            break;
        }
        case ("ERROR") : {
            message = "Проблема с печатью";
            break;
        }
    }

    http.PRINTER_ERROR({text: message});

    //ошибка печати, реплика и переход на экран ошибки
    robot.dialogService.sayReplic("ticket-unavailible");

    //showError("Ошибка принтера! Обратитесь к администратору.");

    return true;
}

//вызов печати талона
function letPrintTicket() {
    robot.printService.status().then(state => {

        if (handlePrinterError(state)) {
            return false;
        }

        let now = new Date;

        let printData = {
            service: myApp.userService,
            offers: myApp.checkedOffers,
            date: `${now.getDate()}.${('0' + (now.getMonth()+1)).slice(-2)}.${now.getFullYear()}`,
            time: `${now.getHours()}:${now.getMinutes()}:${('0' + (now.getSeconds())).slice(-2)}`
        };

        printTalon(printData);

    }).catch(error => {
        handlePrinterError(error);
    });
}

//печать талона
async function printTalon(data) {
    let image = await prepareTalon(data);
    console.log(image);

    robot.dialogService.sayReplic("get-ticket");

    robot.printService.print(image).then(nothing => {
        //успешно
        writeStatistic(myApp.statistic, "OK");

    }).catch(error => {
        robot.printService.status().then(state => {
            //ошибка, статус
            writeStatistic(myApp.statistic, state);

            if (state === "IDLE" || state === "IN_PROCESS") {
                // robot.dialogService.sayReplic("printing_success2");
                return;
            }
            handlePrinterError(state);
        });

    });
}

//подготовка талона к печати
async function prepareTalon(data) {
    let canvas = document.createElement("canvas");
    canvas.width = 604;
    canvas.height = 904;

    let talon = myApp.talonTemplate;

    talon = talonReplace(talon, "{op}", data["service"]);

    talon = talonReplace(talon, "{tn}", myApp.ticketCounter.update());

    if (Object.keys(data.offers).length) {
        let of = '';

        Object.keys(data.offers).map(item => {
            of += item + '<br>';
        });

        talon = talonReplace(talon, "{of}", of);
    }

    talon = talonReplace(talon, "{footer}", data["footer"]);
    talon = talonReplace(talon, "{time}", data["time"]);
    talon = talonReplace(talon, "{date}", data["date"]);

    await rasterizeHTML.drawHTML(talon, canvas);

    return canvas.toDataURL("image/png");
}

//загрузка шаблона
async function loadTalonTemplate() {

    let xhr = new XMLHttpRequest()
    xhr.open("GET", "talon-template.html", true);
    xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                myApp.talonTemplate = xhr.responseText
            }
        }
    };

    xhr.send();
}

//вставка данных в строки шаблона
function talonReplace(template, pattern, value) {
    if (!value || value == "") {
        return template.split(pattern).join("");
    }

    return template.split(pattern).join(value);
}

function TicketCounter() {
    let fn = this;
    const NOW = new Date();
    const TODAY = new Date(new Date(NOW.getFullYear(), NOW.getMonth(), NOW.getDate()));
    let SAVED = localStorage.getItem('today');

    fn.addDate = function (saved, today) {
        if (saved !== today) {
            localStorage.setItem('today', today);
            localStorage.setItem('tc', 0);
        }
    };

    fn.restart = function (today) {
        localStorage.setItem('today', today);
        localStorage.setItem('tc', 0);
    };

    fn.update = function () {
        let tc = parseInt(localStorage.getItem('tc'));
        let n = tc + 1;
        localStorage.setItem('tc', n);

        if (n < 10 && n < 100) {
            n = '00' + n;
        } else if (n > 10 && n < 100) {
            n = '0' + n;
        }

        return n;
    };

    if (SAVED !== null) {
        fn.addDate(SAVED, TODAY);
    } else {
        fn.restart(TODAY);
    }
}

//ошибки выдачи талонов

// function noTalons(data){
//     if (data.error_code == 37){
//         showError("Нет доступных талонов!");
//         return true
//     }
//     return false;
// }

