const SCREENS_PRESALE = {
    //Э 01
    "main": {
        text: {
            title: 'Выберите или скажите какая операция вас интересует',
        },
        htmlBlock: 'start-menu',
        nav: {
            goStart: false,
            goEnd: false
        }
    },
    //Э 04
    "consultant": {
        text: {
            title: 'Хотите решить вопрос без ожидания очереди?',
            plh1: 'Уважаемый клиент, вы  можете не ждать очереди и выполнить операцию в Личном кабинете. \n' + 'Наш консультант поможет вам в этом',
            plh2: 'Хотите продолжить обслуживание с консультантом?'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Да',
                action: 'consultant-accepted',
            },
            {
                text: 'Нет',
                action: 'consultant-rejected',
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 05
    "consultation": {
        text: {
            title: 'Пожалуйста, подойдите к консультанту',
            plh1: 'Пожалуйста, обратитесь к консультанту в зале и он поможет вам оперативно решить вопрос.',
            plh2: ''
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 05_2
    "wait": {
        text: {
            title: 'Пожалуйста, ожидайте очереди',
            plh1: 'Если вы еще не взяли талон, то можете получить его в терминале на входе в отделение.',
            plh2: ''
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 09
    "question": {
        text: {
            title: 'Задайте мне вопросы о Сбербанке',
            plh1: 'Могу я быть еще чем-то полезен?',
            plh2: 'Спросите меня. Я готов ответить на вопросы о Сбербанке.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 10
    "rate": {
        text: {
            title: 'Пожалуйста, оцените мою работу',
        },
        htmlBlock: 'rate',
        nav: {
            goStart: false,
            goEnd: true
        }
    },
    //Э 11
    "bye": {
        text: {
            title: 'Всего доброго',
        },
        htmlBlock: 'bye-screen',
        nav: {
            goStart: true,
            goEnd: false
        }
    },
};

const SCREENS_SALE = {
    //Э 01
    "main": {
        text: {
            title: 'Выберите или скажите какая операция вас интересует',
        },
        htmlBlock: 'start-menu',
        nav: {
            goStart: false,
            goEnd: false
        }
    },
    //Э 04
    "consultant": {
        text: {
            title: 'Хотите решить вопрос без ожидания очереди?',
            plh1: 'Уважаемый клиент, вы  можете не ждать очереди и выполнить операцию в Личном кабинете. \n' + 'Наш консультант поможет вам в этом',
            plh2: 'Хотите продолжить обслуживание с консультантом?'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Да',
                action: 'want-offers',
            },
            {
                text: 'Нет',
                action: 'only-want-offers',
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 05_1
    "get-offers": {
        text: {
            title: 'Узнайте о персональных предложениях',
            plh1: 'Пожалуйста, обратитесь к консультанту в зале и он поможет вам.',
            plh2: 'А пока хотите узнать о доступных для вас спецпредложениях?'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Cпецпредложения',
                action: 'get-tel'
            },
            {
                text: 'Спасибо, не надо',
                classes: 'btn_nope',
                action: 'bye'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 05_1
    "only-get-offers": {
        text: {
            title: 'Узнайте о персональных предложениях',
            plh1: 'А хотите я расскажу о доступных для вас спецпредложениях?',
            plh2: ''
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Cпецпредложения',
                action: 'get-tel'
            },
            {
                text: 'Спасибо, не надо',
                classes: 'btn_nope',
                action: 'bye'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 05_2
    "wait": {
        text: {
            title: 'Пожалуйста, ожидайте очереди',
            plh1: 'Если вы еще не взяли талон, то можете получить его в терминале на входе в отделение.',
            plh2: ''
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 06
    "tel": {
        text: {
            title: 'Узнайте о персональных предложениях',
            plh1: '',
            plh2: ''
        },
        htmlBlock: 'get-tel',
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 08
    "print": {
        text: {
            title: 'Пожалуйста, возьмите талон и подойдите к консультанту',
            plh1: '',
            plh2: ''
        },
        htmlBlock: 'print-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 08_1
    "print-error": {
        text: {
            title: 'Пожалуйста, подойдите к консультанту',
            plh1: 'Извините, не могу напечатать вам талон.',
            plh2: 'Пожалуйста, подойдите к консультанту для оформления персональных предложений и решения вашего вопроса.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: false,
            goEnd: true
        }
    },
    //Э 08_2
    "offers-nouser": {
        text: {
            title: 'Пожалуйста, подойдите к консультанту',
            plh1: 'К сожалению, мне недоступна информация о доступных вам предложениях.',
            plh2: 'Пожалуйста, подойдите к консультанту для решения вопроса, с которым вы пришли.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 08_3
    "offers-unavailable": {
        text: {
            title: 'Пожалуйста, подойдите к консультанту',
            plh1: 'На текущий момент, предложения для вас подготавливаются.',
            plh2: 'Пожалуйста, подойдите к консультанту для оформления персональных предложений и решения вашего вопроса.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    'watched-not-interesting': {
        text: {
            title: 'Пожалуйста, подойдите к консультанту',
            plh1: 'Очень жаль, что вы не заинтересовались нашими спецпредложениями.',
            plh2: 'Пожалуйста, подойдите к консультанту для решения вопроса, с которым вы пришли.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
            {
                text: 'Спросите меня',
                action: 'ask-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 09
    "question": {
        text: {
            title: 'Задайте мне вопросы о Сбербанке',
            plh1: 'Могу я быть еще чем-то полезен?',
            plh2: 'Спросите меня. Я готов ответить на вопросы о Сбербанке.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 10
    "rate": {
        text: {
            title: 'Пожалуйста, оцените мою работу',
        },
        htmlBlock: 'rate',
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 11
    "bye": {
        text: {
            title: 'Всего доброго',
        },
        htmlBlock: 'bye-screen',
        nav: {
            goStart: true,
            goEnd: false
        }
    },
};

let branch = 'sale';
let SCREENS = SCREENS_PRESALE;

if (branch === 'sale') {
    SCREENS = SCREENS_SALE;
}

let notimer = false;

if (localTest === undefined) {
    var localTest = false;
}

let myApp = {
    screens: document.querySelectorAll('.content-wrap'),
    actionPlh: {
        title: document.querySelector('#screen-title'),
        plh1: document.querySelector('#plh1'),
        plh2: document.querySelector('#plh2')
    },
    modal: {
        overlay: document.querySelector('#overlay'),
        window: document.querySelector('#modal'),
        txt: document.querySelector('#modal-txt'),
        btns: document.querySelector('#modal-btns')
    },
    navBtns: {
        goStart: document.querySelector('#go-start'),
        goEnd: document.querySelector('#go-end')
    },
    subtitles: {
        el: document.querySelector('#subtitles'),
        longTxtClass: 'is-long-txt'
    },
    timeout: {
        clearSub: null,
        telIdle: null
    },
    curScreen: 'main',
    userService: '',
    checkedOffers: {},
    tel: document.querySelector('#tel'),
    muteBtn: document.querySelector('#btn-mute'),
    statistic: {
        user: null,
        rate: null,
        tel: null,
        offers: {},
        fio: null
    },

    handleUserAction: function (action, obj = SCREENS, scActions = screenActions) {

        if (myApp.hasOwnProperty('ModalDialog')) {
            myApp.ModalDialog.closeDialog();
        }

        myApp.clearSub();

        let data = obj[action];

        if (!!data) {
            let currentBlock = changeBlock(data.htmlBlock, myApp.screens);

            if (scActions.hasOwnProperty(action)) {
                scActions[action](myApp);
            }

            if (data.hasOwnProperty('text')) {
                changeText(data.text, myApp.actionPlh);
            }

            if (data.hasOwnProperty('nav')) {
                for (let btn in data.nav) {
                    if (data.nav[btn] === true) {
                        myApp.navBtns[btn].removeAttribute('hidden');
                    } else {
                        myApp.navBtns[btn].setAttribute('hidden', 'hidden');
                    }
                }
            }

            if (data.hasOwnProperty('btns')) {
                let btnBar = currentBlock.querySelector('.button-bar');
                let nb;

                removeSiblings(btnBar);

                for (let i = 0; i < data.btns.length; i++) {
                    nb = document.createElement('button');
                    nb.classList.add('btn');
                    nb.classList.add('js-action');
                    nb.textContent = data.btns[i].text;
                    nb.dataset.action = data.btns[i].action;
                    if (data.btns[i].classes) {
                        nb.classList.add(data.btns[i].classes);
                    }
                    btnBar.appendChild(nb);

                    nb.addEventListener('click', function (e) {
                        robot.dialogService.sayReplic(this.dataset.action);
                        e.preventDefault();
                    });
                }
            }
        }

        function changeBlock(blockId = 'start-menu', screens) {

            screens.forEach((item) => {
                item.classList.add('is-hidden');
            });

            let currentBlock = document.getElementById(blockId);

            currentBlock.classList.remove('is-hidden');

            return currentBlock;
        }

        function changeText(txts, elems) {
            for (let txt in txts) {
                if (txts.hasOwnProperty(txt)) {
                    elems[txt].textContent = txts[txt];
                }
            }
        }
    },

    clearSub: function () {
        myApp.subtitles.el.classList.remove(myApp.subtitles.longTxtClass);
        myApp.subtitles.el.textContent = '';
    },

    showSub: function (txt) {
        myApp.clearSub();

        let k = findSubStringsRatio(txt, myApp.subtitles.charW);

        if (k >= 2) {
            myApp.subtitles.el.classList.add(myApp.subtitles.longTxtClass);
        }

        if (k >= 6) {
            txt = kitcut(txt, 430);
        }

        myApp.subtitles.el.textContent = txt;
    },

    muteRobot: function (muted = true) {
        if (this.muteBtn) {
            this.muteBtn.addEventListener('click', function () {
                robot.dialogService.abortRobotReplic();
                this.classList.add('ico-mic-muted');
            })
        }
    }
};

const TIMEOUT = {
    setRate: {
        fn: function (text = 'bye') {
            console.log('timeout set rate: ', text);

            //это реплика связанная с action - переход на последний экран
            robot.dialogService.sayReplic(text);

            myApp.timeout.setRate = null;
        },
        t: 1000
    },
    clearSub: {
        fn: myApp.clearSub,
        t: 5000
    },
    printRateMe: {
        fn: function () {
            robot.dialogService.sayReplic('rate-me');

            myApp.timeout.printRateMe = null;
        },
        t: 4000
    },
    gotoMain: {
        fn: function () {
            robot.dialogService.sayReplic('back-to-main');

            myApp.timeout.gotoMain = null;
        },
        t: 4000
    },
    telIdle: {
        fn: function () {
            console.log('tel idle start');
            let field = document.querySelector('#tel');

            if (field.value.length === 0) {
                robot.dialogService.sayReplic('tel-timer');

                if (localTest) {
                    robot.stub.robotReplicStart('tel-timer');
                }
            } else if (field.value.length === 15) {
                robot.dialogService.sayReplic('click-offers');

                if (localTest) {
                    robot.stub.robotReplicStart('click-offers');
                }
            }

            myApp.timeout.telIdle = setTimeout(TIMEOUT.telIdle.fn, TIMEOUT.telIdle.t);
        },
        t: 20000
    }
};

let screenActions = {
    tel: function () {
        document.querySelector('#tel').focus();

        if (!notimer) {
            console.log('user idle: tel-timer ');
            myApp.timeout.telIdle = setTimeout(TIMEOUT.telIdle.fn, TIMEOUT.telIdle.t);
        }
    },
    print: function () {
        myApp.timeout.printRateMe = setTimeout(TIMEOUT.printRateMe.fn, TIMEOUT.printRateMe.t);
    },
    bye: function () {
        robot.custom.send({"caseFinished": true});
        myApp.timeout.gotoMain = setTimeout(TIMEOUT.gotoMain.fn, TIMEOUT.gotoMain.t);
    }
};

function ShowDialog(appData) {
    let fn = this;
    const CLASSES = {'nope': 'btn_nope', 'wide': 'modal-btn_wide'};

    fn.openDialog = function (text) {
        appData.modal.overlay.classList.add('is-open');
        appData.modal.window.classList.add('is-open');
        robot.dialogService.sayReplic(text);
    };

    fn.closeDialog = function () {
        appData.modal.overlay.classList.remove('is-open');
        appData.modal.window.classList.remove('is-open');
        appData.modal.txt.textContent = '';

        removeSiblings(appData.modal.btns);
    };

    fn.createWindow = function (modalData, handler) {
        appData.modal.txt.textContent = modalData.text;

        if (modalData.answers) {
            removeSiblings(appData.modal.btns);

            let wideBtn = modalData.answers.length > 2;

            for (let val in modalData.answers) {

                let btn = document.importNode(document.querySelector('#tmpl-btn').content, true).querySelector('button');

                if (modalData.answers[val] === 'Нет') {
                    btn.classList.add(CLASSES.nope);
                }

                if (wideBtn) {
                    btn.classList.add(CLASSES.wide);
                }

                btn.textContent = modalData.answers[val];

                appData.modal.btns.appendChild(btn);

                btn.onclick = event => {
                    handler(btn.textContent);
                    robot.custom.send({"userClick": true});
                }
            }

            fn.openDialog(modalData.text);
        }
    };
}

//произнести реплику и вызвать обработчик по клику на элемента
//опять не понял - нужен ли тут обработчик
//или только реплика и ждать onAction
function setTouchEvents(selector = '.js-action') {
    let elems = document.querySelectorAll(selector);

    for (let el of elems) {
        el.addEventListener('click', function () {
            let act = this.dataset.action;

            if (act) {
                robot.dialogService.sayReplic(act);

                robot.custom.send({"userClick": true});
            }
        })
    }
}

function addStartStopNavEvents() {
    const beginBtn = document.querySelector('#go-start');
    const finishBtn = document.querySelector('#go-end');

    beginBtn.addEventListener('click', function () {

        robot.dialogService.sayReplic('findout-visit-property');
        robot.custom.send({"caseFinished": true});
        robot.custom.send({"caseStarted": true});

        //очистка поля телефон
        //TODO: разобраться куда пропадает myApp.tel
        document.querySelector('#tel').value = '';
        // if (myApp.hasOwnProperty('tel') && myApp.tel) {
        //     myApp.tel.value = '';
        // }

        //очистка слайдов персональных предложений
        if (myApp.hasOwnProperty('offersSlider')) {
            myApp.offersSlider.removeSlides();
        }

        //очистка статистики полученных ПП
        cleanOffersStatistic(myApp);

        //очистка субтитров
        myApp.clearSub();

        //очистка рейтинга
        RobotRating.clearRate();

        myApp.statistic.user = null;
        myApp.statistic.rate = null;

    });

    finishBtn.addEventListener('click', function () {
        document.querySelector('#tel').value = '';
        // if (myApp.hasOwnProperty('tel') && myApp.tel) {
        //     myApp.tel.value = '';
        // }

        //очистка статистики полученных ПП
        cleanOffersStatistic(myApp);

        robot.dialogService.sayReplic('bye');
    })
}

function Rating() {
    let fn = this;
    let rate = document.querySelector('#rate');
    let btns = rate.querySelectorAll('.rate-item');

    fn.setRate = function () {
        for (let btn of btns) {
            btn.addEventListener('click', function () {
                if (fn.setSelected(this)) {
                    robot.custom.send({"userClick": true});

                    //это реплика-оценка
                    let rateTxt = btn.dataset.say;
                    //robot.dialogService.sayReplic(rateTxt);
                    myApp.statistic.rate = rateTxt.replace('bye-', '');

                    //запись статистики
                    writeStatistic(myApp.statistic);

                    myApp.timeout.setRate = setTimeout(TIMEOUT.setRate.fn, TIMEOUT.setRate.t, rateTxt);
                }
            })
        }
    };

    fn.clearRate = function () {
        for (let btn of btns) {
            btn.classList.remove('is-selected');
        }
    };

    fn.setSelected = function (btn) {
        let selected = rate.querySelectorAll('.is-selected');

        if (selected.length) {
            return false;
        }

        btn.classList.add('is-selected');

        return true;
    };
}

function handleRate(rate) {
    let rateBtn = document.querySelector('[data-say=bye-' + rate + ']');
    rateBtn.click();
}

function writeStatistic(data) {
    let stata = formatDate(new Date) + ";"
        + data.user + ";"
        + data.rate;

    robot.statistic(
        stata, "sale"
    )
}

function writeOffersStatistic(data) {

    for (let key in data.offers) {
        let stata = data.tel;
        stata += ';' + data.fio;
        stata += ';' + data.user;
        stata += ';' + formatDate(new Date);
        stata += ';' + key;

        if (data.offers[key]) {
            stata += '; выбрано';
        } else {
            stata += '; не выбрано';
        }

        if (localTest) {
            console.log(stata);
        }

        robot.statistic(
            stata, "watched_offers_result"
        )
    }
}

function cleanOffersStatistic(mainObj) {
    mainObj.offers = null;
    mainObj.tel = null;
    mainObj.fio = null;
    mainObj.checkedOffers = {};
}

document.addEventListener('DOMContentLoaded', function () {
    //загрузка шаблона талона
    loadTalonTemplate();

    //запуск счетчика талонов
    myApp.ticketCounter = new TicketCounter;

    myApp.screens = document.querySelectorAll('.content-wrap');

    myApp.actionPlh = {
        title: document.querySelector('#screen-title'),
        plh1: document.querySelector('#plh1'),
        plh2: document.querySelector('#plh2')
    };

    myApp.subtitles.charW = findCharWidth('Е');

    //запись какую услугу выбрали на главном экране
    let userService = document.querySelectorAll('.js-service');

    for (let el of userService) {
        el.addEventListener('click', function () {
            myApp.userService = el.querySelector('.card__title').textContent;
        })
    }

    setTouchEvents();
    addStartStopNavEvents();

    RobotRating = new Rating;

    RobotRating.setRate();

    myApp.ModalDialog = new ShowDialog(myApp);

    let robotInstance = 'getInstance';

    if (localTest) {
        robotInstance = 'getStubInstance';
    }

    Promobot[robotInstance]().then((promobot) => {

        robot = promobot;

        promobot.userService.onUserPresence((user, isNew) => {
            console.log("is new", isNew, "user : ", user);
            myApp.statistic.user = user.user_id;
        });

        promobot.userService.onUserLost(no => {
            console.log("userLost");
        });

        promobot.dialogService.onUserReplic(text => {
            console.log("user said " + text);

            promobotOnUserSay(text);

        });

        promobot.custom.onEvent(data => {
            console.log("custom event", data);

            //case start
            if (data.startCase === true) {
                robot.dialogService.sayReplic('findout-visit-property');

                //очистка поля телефон
                document.querySelector('#tel').value = '';
                // if (myApp.hasOwnProperty('tel') && myApp.tel) {
                //     myApp.tel.value = '';
                // }

                //очистка слайдов персональных предложений
                if (myApp.hasOwnProperty('offersSlider')) {
                    myApp.offersSlider.removeSlides();
                }

                //очистка статистики полученных ПП
                cleanOffersStatistic(myApp);

                //очистка субтитров
                myApp.clearSub();

                //очистка рейтинга
                RobotRating.clearRate();

                myApp.statistic.user = null;
                myApp.statistic.rate = null;
            }

            //case finish
            if (data.stopCase === true) {
                cleanOffersStatistic(myApp);
                document.querySelector('#tel').value = '';
                robot.dialogService.sayReplic('bye');
            }
        });

        promobot.dialogService.onRobotReplicStart(text => {
            console.log("robot start replic " + text);

            promobotReplicaStart(text);
        });

        promobot.dialogService.onRobotReplicFinish(nothing => {
            console.log("robot finish replic ", nothing);

            promobotReplicaFinish();
        });

        promobot.dialogService.onQuestion(question => {
                myApp.ModalDialog.createWindow(question, answer => {
                    question.setAnswer(answer);
                    myApp.ModalDialog.closeDialog();
                });
            },
            nothing => {
                myApp.ModalDialog.closeDialog();
            }
        );

        promobot.dialogService.onAction(action => {
            console.log("onAction", action);

            promobotOnAction(action);
        })
    });

    myApp.muteRobot();

    //для sale
    if (branch === 'sale') {
        let telInput = myApp.tel;

        telInput.addEventListener('input', mask, false);
        telInput.addEventListener('focus', mask, false);
        telInput.addEventListener('blur', mask, false);

        validateTelInput(telInput);

        myApp.offersPopup = new OffersPopup(myApp);
    }
});

//--robot API events start-----

function promobotOnUserSay(text) {
    clearTimeout(myApp.timeout.clearSub);

    myApp.showSub(clearSubString(text));

    myApp.timeout.clearSub = setTimeout(TIMEOUT.clearSub.fn, TIMEOUT.clearSub.t);

    switch (myApp.curScreen) {
        case 'print':
            stopTimeout('printRateMe', 'pause');
            break;
        case 'bye':
            stopTimeout('gotoMain', 'pause');
            break;
        case 'tel':
            if (myApp.timeout.telIdle !== null) {
                console.log('telIdle', myApp.timeout.telIdle);
                stopTimeout('telIdle', 'pause');
            }
            break;
    }
}

function promobotReplicaStart(text) {
    myApp.showSub(clearSubString(text));

    clearTimeout(myApp.timeout.clearSub);

    switch (myApp.curScreen) {
        case 'print':
            stopTimeout('printRateMe', 'pause');
            break;
        case 'bye':
            stopTimeout('gotoMain', 'pause');
            break;
        case 'tel':
            if (myApp.timeout.telIdle !== null) {
                console.log('telIdle', myApp.timeout.telIdle);
                stopTimeout('telIdle', 'pause');
            }
            break;
    }

    if (localTest) {
        setTimeout(function () {
            robot.stub.robotReplicFinish();
        }, 2000);
    }
}

function promobotReplicaFinish() {

    myApp.clearSub();
    myApp.muteBtn.classList.remove('ico-mic-muted');

    clearTimeout(myApp.timeout.clearSub);

    switch (myApp.curScreen) {
        case 'print':
            if (myApp.timeout.printRateMe === 'pause') {
                setTimeout(TIMEOUT.printRateMe.fn, TIMEOUT.printRateMe.t);
            }
            break;
        case 'bye':
            if (myApp.timeout.gotoMain === 'pause') {
                setTimeout(TIMEOUT.gotoMain.fn, TIMEOUT.gotoMain.t);
            }
            break;
        case 'tel':
            if (myApp.timeout.telIdle === 'pause') {
                console.log('re-run telIdle');
                myApp.timeout.telIdle = setTimeout(TIMEOUT.telIdle.fn, TIMEOUT.telIdle.t);
            }
            break;
    }
}

function promobotOnAction(action) {

    if (Array.isArray(action)) {

        for (let i = 0; i < action.length; i++) {
            if (action[i] === 'call-step-phrase') {
                let phrase = action[i + 1];

                console.log('onAction phrase', phrase);
                robot.dialogService.sayReplic(phrase);

                delete action[i];
                delete action[i + 1];
            }

            if (action[i] === 'screen') {
                let screenName = action[i + 1];

                myApp.curScreen = screenName;

                //при переходе на другой экран очищаются все запущенные таймеры
                for (let timer in myApp.timeout) {
                    stopTimeout(timer);
                }

                if(myApp.offersPopup && document.querySelector('#offers-popup').classList.contains('is-open')){
                    myApp.offersPopup.close();
                }

                //открыть попап голосом
                if (screenName === 'offers-popup') {
                    let slide = parseInt(action[i + 2]);

                    myApp.offersSlider.gotoSlide(slide - 1);
                    myApp.offersPopup.open();

                    delete action[i];
                    delete action[i + 1];
                    delete action[i + 2];
                } else {
                    myApp.handleUserAction(screenName);

                    delete action[i];
                    delete action[i + 1];
                }
            }

            if (action[i] === 'rate') {
                console.log('onAction rate', action[i + 1]);
                handleRate(action[i + 1]);

                delete action[i];
                delete action[i + 1];
            }

            if (action[i] === 'set-offer') {
                let act = action[i + 1];

                if(myApp.curScreen === 'tel' && document.querySelector('#offers-popup').classList.contains('is-open')){
                    let slide = myApp.offersSlider.getCurSlide();

                    if(act === 'interesting'){
                        slide.querySelectorAll('.js-offer-y')[0].click();
                    }

                    if(act === 'not-interesting'){
                        slide.querySelectorAll('.js-offer-n')[0].click();
                    }
                }

                delete action[i];
                delete action[i + 1];
            }


            if (action[i] !== undefined) {
                console.log('onAction default', action[i]);
                myApp.handleUserAction(action[i + 1]);
            }
        }
    } else {
        myApp.handleUserAction(action);
    }
}

//--robot API events end-----

//sale
function validateTelInput(field) {
    const ERRORS = ['Поле обязательно для заполнения', 'Пожалуйста, введите телефон правильно'];
    let telBlock = document.querySelector('#get-tel');
    let btn = telBlock.querySelector('#btn-get-tel');
    let formCol = telBlock.querySelector('.form-col');
    let colError = formCol.querySelector('.error-desc');

    field.addEventListener('focus', function () {
        clearColError(formCol, colError);
    });

    field.addEventListener('keyup', function () {
        robot.custom.send({"userClick": true});

        if (!notimer) {
            stopTimeout('telIdle', 'pause');

            myApp.timeout.telIdle = setTimeout(TIMEOUT.telIdle.fn, TIMEOUT.telIdle.t);
        }
    });

    btn.addEventListener('click', function () {
        robot.custom.send({"userClick": true});

        clearColError(formCol, colError);

        if (checkField(field)) {
            let telno = '7' + field.value.trim();
            telno = telno.replace(' ', '');
            telno = telno.replace('(', '');
            telno = telno.replace(')', '');
            telno = telno.replace(/-/g, '');

            myApp.offersCards = new CreateOfferCard(telno);
        }
    });

    function checkField(field, showError = true) {
        let check = true;

        if (field.value.length === 0) {
            if (showError) {
                showColError(formCol, colError, ERRORS[0]);

                robot.dialogService.sayReplic('tel-timer');

                if (localTest) {
                    robot.stub.robotReplicStart('tel-timer');
                }
            }

            check = false;
        }

        //15 - это вместе с пробелами, скобками и прочим мусором
        if (field.value.length > 0 && field.value.length < 15) {
            if (showError) {
                showColError(formCol, colError, ERRORS[1]);

                robot.dialogService.sayReplic('click-offers-error');

                if (localTest) {
                    robot.stub.robotReplicStart('click-offers-error');
                }
            }

            check = false;
        }

        return check;
    }

    function showColError(col, error, errorDesc) {
        error.textContent = errorDesc;
        col.classList.add('col-error');
    }

    function clearColError(col, error) {
        error.textContent = '';
        col.classList.remove('col-error');
    }
}

function CreateOfferCard(tel) {
    let offersSliderWrap = document.querySelector('#offers-slider');
    let offersSlider = offersSliderWrap.querySelector('.slider__content');
    let sliderPag = offersSliderWrap.querySelector('.slider-pag');
    const IMGPATH = 'offers-pics/';
    let offers = new PersonalOffers("http://localhost:8080/");

    offers.get(tel, 3).then(data => {
        // get-offers - юзер есть в базе, есть предложения
        // wait-offers - нет предложений,
        // no-offers - юзер не найден

        if (Object.keys(data).length) {

            myApp.statistic.tel = tel;

            if (data.offers.length) {
                if (myApp.offersSlider) {
                    myApp.offersSlider.removeSlides();
                }

                //очистка статистики полученных ПП
                cleanOffersStatistic(myApp);

                function goNext(slide) {
                    robot.dialogService.abortRobotReplic();

                    let index = myApp.offersSlider.isLast(parseInt(slide.dataset.index));

                    if (index !== 'last') {
                        setTimeout(function () {
                            myApp.offersSlider.gotoSlide(index + 1);

                            let cur = sliderPag.querySelector('.cur');
                            cur.classList.remove('cur');
                            cur.nextElementSibling.classList.add('cur');
                        }, 1500);
                    } else {
                        setTimeout(function () {
                            if (Object.keys(myApp.checkedOffers).length) {
                                letPrintTicket();
                            } else {
                                robot.dialogService.sayReplic('watched-not-interesting');

                                if (localTest) {
                                    robot.stub.robotReplicStart('watched-not-interesting')
                                }
                            }

                            setTimeout(function () {
                                myApp.offersPopup.close();
                            }, 1500);

                        }, 1500);
                    }
                }

                let count = -1;

                for (let item of data.offers) {
                    count++;

                    myApp.statistic.offers[item.title] = false;

                    if (data.offers.length > 1) {
                        let pagItem = document.createElement('div');
                        sliderPag.appendChild(pagItem);

                        if (count === 0) {
                            pagItem.classList.add('cur');
                        }
                    }

                    let slide = document
                        .importNode(document.querySelector('#tmpl-offer-slide').content, true)
                        .querySelector('.offer-slide');

                    let imgName = item.picture;

                    if (imgName < 10) {
                        imgName = '0' + imgName;
                    }

                    slide.querySelector('.offer-slide__title').textContent = item.title;
                    slide.querySelector('img').src = `${IMGPATH}${imgName}.png`;
                    slide.dataset.say = item.replic;
                    slide.dataset.index = count;

                    let btnInterest = slide.querySelector('.js-offer-y');
                    btnInterest.dataset.offer = item.title;

                    let btnNext = slide.querySelector('.js-offer-n');

                    btnInterest.addEventListener('click', function (e) {
                        robot.custom.send({"userClick": true});

                        this.classList.add('is-selected');

                        myApp.statistic.offers[item.title] = true;
                        myApp.checkedOffers[item.title] = true;

                        goNext(slide);

                        e.preventDefault();
                    });

                    btnNext.addEventListener('click', function (e) {
                        robot.custom.send({"userClick": true});

                        this.classList.add('is-selected');

                        goNext(slide);

                        e.preventDefault();
                    });

                    offersSlider.appendChild(slide);
                }

                let firstPhrase = data.user;

                if (data.offers.length > 1) {
                    let lang = ['два', 'три', 'четыре'];
                    firstPhrase += ', вам доступно ' + lang[data.offers.length - 2] + ' персональных предложения.     '
                } else {
                    firstPhrase += ', вам доступно персональное предложение.    ';
                }

                firstPhrase += data.offers[0].replic;

                stopTimeout('telIdle');

                myApp.offersSlider = new OffersSlider;

                robot.dialogService.abortRobotReplic();
                myApp.offersPopup.open();

                setTimeout(function () {
                    robot.dialogService.sayReplic('set_offer_anchor');
                    robot.dialogService.sayText(firstPhrase);

                    if (localTest) {
                        console.log(firstPhrase);
                    }
                }, 1000);


            } else {
                robot.dialogService.sayReplic('wait-offers');
                robot.custom.send({userName: {"id": myApp.statistic.user, "name": data.user}});
            }

            myApp.statistic.fio = data.user;
        } else {
            robot.dialogService.sayReplic('no-offers');
        }
    });
}

function OffersSlider() {
    let fn = this;
    let slider = document.querySelector('#offers-slider');
    let sliderContent = slider.querySelector('.slider__content');
    let slides = slider.querySelectorAll('.slide');
    let slidesNum = slides.length - 1;

    fn.isLast = function (index) {

        if (index === slidesNum) {
            return 'last';
        }

        return index;
    };

    //TODO: выпилить это угребище
    fn.getCurIndex = function (slider = sliderContent) {
        let res = 0;


        let str = slider.style.transform;

        if (!str.length) {
            return res;
        }

        let n = str.indexOf("(");
        let n1 = str.indexOf(")");

        res = parseInt(str.slice(n + 1, n1 - 1));

        return Math.abs(res / 100);
    };

    fn.gotoSlide = function (index = 0) {

        sliderContent.style.transform = 'translateX(-' + (100 * index) + '%)';

        setTimeout(function () {
            robot.dialogService.sayReplic('set_offer_anchor');
            robot.dialogService.sayText(sliderContent.children[index].dataset.say);

            if (localTest) {
                console.log(sliderContent.children[index].dataset.say);
            }
        }, 1000);
    };

    fn.removeSlides = function () {
        sliderContent.removeAttribute('style');
        removeSiblings(sliderContent);

        for (let i of slider.querySelectorAll('.slider-pag > div')) {
            i.remove();
        }
    };

    fn.getCurSlide = function () {
        let index = fn.getCurIndex();
        return slides[index];
    }
}

function OffersPopup(mainObj) {
    let fn = this;

    let popup = document.querySelector('#offers-popup');


    fn.open = function () {
        popup.classList.add('is-open');
        mainObj.modal.overlay.classList.add('is-open', 'is-dark');
    };

    fn.close = function () {
        popup.classList.remove('is-open');
        mainObj.modal.overlay.classList.remove('is-open', 'is-dark');
    };
}

function stopTimeout(timeout, state = null) {
    //console.log('stop timeout: ', timeout);

    clearTimeout(myApp.timeout[timeout]);
    delete myApp.timeout[timeout];
    myApp.timeout[timeout] = state;
}

//----talon----

//обработка ошибок печати
function handlePrinterError(error) {
    let message = "test promobot";

    switch (error) {
        case ("IDLE") : {
            return false;
        }
        case ("IN_PROCESS") : {
            //showError("Принтер занят. Пожалуйста, повторите операцию через несколько секунд.");
            return true;
        }
        case ("LOW_PAPER") : {
            // message = settings.message_printer_paper_near_end.split("{kiosk_title}").join(host);
            // http.PRINTER_ERROR({text: message});
            return false;
        }
        case ("NOT_CONNECTED") : {
            message = "Не подключен принтер";
            break;
        }
        case ("NO_PAPER") : {
            message = settings.message_kiosk_no_paper;
            break;
        }
        case ("ERROR") : {
            message = "Проблема с печатью";
            break;
        }
    }

    // http.PRINTER_ERROR({text: message});

    //ошибка печати, реплика и переход на экран ошибки
    robot.dialogService.sayReplic("ticket-unavailible");

    //showError("Ошибка принтера! Обратитесь к администратору.");

    return true;
}

//вызов печати талона
function letPrintTicket() {
    robot.printService.status().then(state => {

        if (handlePrinterError(state)) {
            return false;
        }

        let now = new Date;

        let printData = {
            service: myApp.userService,
            offers: myApp.checkedOffers,
            date: `${now.getDate()}.${('0' + (now.getMonth() + 1)).slice(-2)}.${now.getFullYear()}`,
            time: `${now.getHours()}:${now.getMinutes()}:${('0' + (now.getSeconds())).slice(-2)}`
        };

        printTalon(printData);

    }).catch(error => {
        if (!handlePrinterError(error)) {
            printTalon(printData);
        }
    });

    if (localTest) {
        let now = new Date;

        let printData = {
            service: myApp.userService,
            offers: myApp.checkedOffers,
            date: `${now.getDate()}.${('0' + (now.getMonth() + 1)).slice(-2)}.${now.getFullYear()}`,
            time: `${now.getHours()}:${now.getMinutes()}:${('0' + (now.getSeconds())).slice(-2)}`
        };

        console.log(printData);

        printTalon(printData);
    }
}

//печать талона
async function printTalon(data) {
    let image = await prepareTalon(data);

    if (localTest) {
        console.log(image);
    }

    robot.dialogService.sayReplic("get-ticket");

    robot.printService.print(image).then(nothing => {
        //успешно
        writeStatistic(myApp.statistic, "OK");
        writeOffersStatistic(myApp.statistic, "OK");

    }).catch(error => {
        robot.printService.status().then(state => {
            //ошибка, статус
            writeStatistic(myApp.statistic, state);
            writeOffersStatistic(myApp.statistic, state);

            if (state === "IDLE" || state === "IN_PROCESS") {
                // robot.dialogService.sayReplic("printing_success2");
                return;
            }
            handlePrinterError(state);
        });

    });
}

//подготовка талона к печати
async function prepareTalon(data) {
    let canvas = document.createElement("canvas");
    canvas.width = 654;
    canvas.height = 904;

    let talon = myApp.talonTemplate;

    // talon = talonReplace(talon, "{op}", data["operation_title"]);
    talon = talonReplace(talon, "{op}", data["service"]);

    talon = talonReplace(talon, "{tn}", myApp.ticketCounter.update());

    if (Object.keys(data.offers).length) {
        let of = '';

        Object.keys(data.offers).map(item => {
            of += item + '<br>';
        });

        talon = talonReplace(talon, "{of}", of);
    }

    talon = talonReplace(talon, "{footer}", data["footer"]);
    talon = talonReplace(talon, "{time}", data["time"]);
    talon = talonReplace(talon, "{date}", data["date"]);

    await rasterizeHTML.drawHTML(talon, canvas);

    return canvas.toDataURL("image/png");
}

//загрузка шаблона
async function loadTalonTemplate() {

    let xhr = new XMLHttpRequest()
    xhr.open("GET", "talon-template.html", true);
    xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                myApp.talonTemplate = xhr.responseText
            }
        }
    };

    xhr.send();
}

//вставка данных в строки шаблона
function talonReplace(template, pattern, value) {
    if (!value || value == "") {
        return template.split(pattern).join("");
    }

    return template.split(pattern).join(value);
}

function TicketCounter() {
    let fn = this;
    const NOW = new Date();
    const TODAY = new Date(new Date(NOW.getFullYear(), NOW.getMonth(), NOW.getDate()));
    let SAVED = localStorage.getItem('today');

    fn.addDate = function (saved, today) {
        if (saved !== today) {
            localStorage.setItem('today', today);
            localStorage.setItem('tc', 0);
        }
    };

    fn.restart = function (today) {
        localStorage.setItem('today', today);
        localStorage.setItem('tc', 0);
    };

    fn.update = function () {
        let tc = parseInt(localStorage.getItem('tc'));
        let n = tc + 1;
        localStorage.setItem('tc', n);

        if (n < 10 && n < 100) {
            n = '00' + n;
        } else if (n > 10 && n < 100) {
            n = '0' + n;
        }

        return n;
    };

    if (SAVED !== null) {
        fn.addDate(SAVED, TODAY);
    } else {
        fn.restart(TODAY);
    }
}

//-utils-----
function setCursorPosition(pos, elem) {
    elem.focus();
    if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
    else if (elem.createTextRange) {
        let range = elem.createTextRange();
        range.collapse(true);
        range.moveEnd("character", pos);
        range.moveStart("character", pos);
        range.select()
    }
}

function mask(event) {
    let matrix = "(___) __-___-__",
        i = 0,
        def = matrix.replace(/\D/g, ""),
        val = this.value.replace(/\D/g, "");
    if (def.length >= val.length) val = def;
    this.value = matrix.replace(/./g, function (a) {
        return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
    });
    if (event.type === "blur") {
        if (this.value.length === 2) this.value = ""
    } else setCursorPosition(this.value.length, this)
}

function formatDate(date) {
    let day = date.getDate() + "";
    let month = (date.getMonth() + 1) + "";
    let year = date.getFullYear() + "";


    if (day.length < 2) {
        day = 0 + day;
    }

    if (month.length < 2) {
        month = 0 + month;
    }

    return day + "." + month + "." + year + " " + ('0' + date.getHours()).slice(-2) + ":" + ('0' + date.getMinutes()).slice(-2) + ":" + ('0' + date.getSeconds()).slice(-2);
}

function removeSiblings(parent) {
    while (parent.firstElementChild) {
        parent.firstElementChild.remove();
    }
}

function clearSubString(str) {
    if (str.length) {
        let clearStr = str.replace(/{[^}]+}/g, '');
        clearStr = clearStr.replace('+', '');
        clearStr = clearStr.replace(/ {1,}/g, " ");

        return clearStr;
    }
}

function findCharWidth(str) {
    let test = document.createElement('div');
    test.classList.add('char-test');
    test.textContent = str;
    document.body.appendChild(test);
    let w = test.clientWidth;
    test.remove();
    return w;
}

function findSubStringsRatio(str, charW, maxpx = 1000) {

    let strW = str.length * charW;

    return strW / maxpx;
}

function kitcut(text, limit) {
    if (text.length <= limit) return text;

    text = text.slice(0, limit);

    let lastSpace = text.lastIndexOf(" ");

    if (lastSpace > 0) {
        text = text.substr(0, lastSpace);
    }

    return text + "...";
}

//-utils-----