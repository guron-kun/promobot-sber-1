const SCREENS_SALE_1 = {
    //Э 01
    "main": {
        text: {
            title: 'Цените свое время? Тогда наши услуги для вас!',
        },
        htmlBlock: 'start-menu',
        nav: {
            goStart: false,
            goEnd: false
        }
    },
    //Э 04
    "consultant": {
        text: {
            title: 'Подойдите к консультанту для подключения услуги',
            plh1: 'Подойдите к консультанту и он поможет вам подключить услугу.',
            plh2: 'При наличии карты Сбербанка с собой'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me',
            },
            {
                text: 'Спросите меня',
                action: 'ask-me',
            }
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 09
    "question": {
        text: {
            title: 'Задайте мне вопросы о Сбербанке',
            plh1: 'Могу я быть еще чем-то полезен?',
            plh2: 'Спросите меня. Я готов ответить на вопросы о Сбербанке.'
        },
        htmlBlock: 'action-screen',
        btns: [
            {
                text: 'Оцените меня',
                action: 'rate-me'
            },
        ],
        nav: {
            goStart: true,
            goEnd: true
        }
    },
    //Э 10
    "rate": {
        text: {
            title: 'Пожалуйста, оцените мою работу',
        },
        htmlBlock: 'rate',
        nav: {
            goStart: false,
            goEnd: true
        }
    },
    //Э 11
    "bye": {
        text: {
            title: 'Всего доброго',
        },
        htmlBlock: 'bye-screen',
        nav: {
            goStart: true,
            goEnd: false
        }
    },
};

if (localTest === undefined) {
    var localTest = false;
}

let SCREENS = SCREENS_SALE_1;

let myApp = {
    screens: document.querySelectorAll('.content-wrap'),
    actionPlh: {
        title: document.querySelector('#screen-title'),
        plh1: document.querySelector('#plh1'),
        plh2: document.querySelector('#plh2')
    },
    modal: {
        overlay: document.querySelector('#overlay'),
        window: document.querySelector('#modal'),
        txt: document.querySelector('#modal-txt'),
        btns: document.querySelector('#modal-btns')
    },
    navBtns: {
        goStart: document.querySelector('#go-start'),
        goEnd: document.querySelector('#go-end')
    },
    subtitles: {
        el: document.querySelector('#subtitles'),
        longTxtClass: 'is-long-txt'
    },
    timeout: {
        clearSub: null,
        telIdle: null,
        offersIdle: null,
        offersPopupIdle: null
    },
    curScreen: 'main',
    userService: '',
    checkedOffers: {},
    muteBtn: document.querySelector('#btn-mute'),
    statistic: {
        user: null,
        rate: null
    },

    handleUserAction: function (action, obj = SCREENS, scActions = screenActions) {

        if (myApp.hasOwnProperty('ModalDialog')) {
            myApp.ModalDialog.closeDialog();
        }

        myApp.clearSub();

        let data = obj[action];

        if (!!data) {
            let currentBlock = changeBlock(data.htmlBlock, myApp.screens);

            if (scActions.hasOwnProperty(action)) {
                scActions[action](myApp);
            }

            if (data.hasOwnProperty('text')) {
                changeText(data.text, myApp.actionPlh);
            }

            if (data.hasOwnProperty('nav')) {
                for (let btn in data.nav) {
                    if (data.nav[btn] === true) {
                        myApp.navBtns[btn].removeAttribute('hidden');
                    } else {
                        myApp.navBtns[btn].setAttribute('hidden', 'hidden');
                    }
                }
            }

            if (data.hasOwnProperty('btns')) {
                let btnBar = currentBlock.querySelector('.button-bar');
                let nb;

                removeSiblings(btnBar);

                for (let i = 0; i < data.btns.length; i++) {
                    nb = document.createElement('button');
                    nb.classList.add('btn');
                    nb.classList.add('js-action');
                    nb.textContent = data.btns[i].text;
                    nb.dataset.action = data.btns[i].action;
                    if (data.btns[i].classes) {
                        nb.classList.add(data.btns[i].classes);
                    }
                    btnBar.appendChild(nb);

                    nb.addEventListener('click', function (e) {
                        robot.dialogService.sayReplic(this.dataset.action);
                        e.preventDefault();
                    });
                }
            }
        }

        function changeBlock(blockId = 'start-menu', screens) {

            screens.forEach((item) => {
                item.classList.add('is-hidden');
            });

            let currentBlock = document.getElementById(blockId);

            currentBlock.classList.remove('is-hidden');

            return currentBlock;
        }

        function changeText(txts, elems) {
            for (let txt in txts) {
                if (txts.hasOwnProperty(txt)) {
                    elems[txt].textContent = txts[txt];
                }
            }
        }
    },

    clearSub: function () {
        myApp.subtitles.el.classList.remove(myApp.subtitles.longTxtClass);
        myApp.subtitles.el.textContent = '';
    },

    showSub: function (txt) {
        myApp.clearSub();

        let k = findSubStringsRatio(txt, myApp.subtitles.charW);

        if (k >= 2) {
            myApp.subtitles.el.classList.add(myApp.subtitles.longTxtClass);
        }

        if (k >= 6) {
            txt = kitcut(txt, 430);
        }

        myApp.subtitles.el.textContent = txt;
    },

    muteRobot: function (muted = true) {
        if (this.muteBtn) {
            this.muteBtn.addEventListener('click', function () {
                robot.dialogService.abortRobotReplic();
                this.classList.add('ico-mic-muted');
            })
        }
    }
};

const TIMEOUT = {
    setRate: {
        fn: function (text = 'bye') {
            console.log('timeout set rate: ', text);

            //это реплика связанная с action - переход на последний экран
            robot.dialogService.sayReplic(text);

            myApp.timeout.setRate = null;
        },
        t: 1000
    },
    clearSub: {
        fn: myApp.clearSub,
        t: 5000
    },
    printRateMe: {
        fn: function () {
            robot.dialogService.sayReplic('rate-me');

            myApp.timeout.printRateMe = null;
        },
        t: 4000
    },
    gotoMain: {
        fn: function () {
            robot.dialogService.sayReplic('back-to-main');

            myApp.timeout.gotoMain = null;
        },
        t: 4000
    }
};

let screenActions = {
    bye: function () {
        robot.custom.send({"caseFinished": true});
        myApp.timeout.gotoMain = setTimeout(TIMEOUT.gotoMain.fn, TIMEOUT.gotoMain.t);
    }
};

function ShowDialog(appData) {
    let fn = this;
    const CLASSES = {'nope': 'btn_nope', 'wide': 'modal-btn_wide'};

    fn.openDialog = function (text) {
        appData.modal.overlay.classList.add('is-open');
        appData.modal.window.classList.add('is-open');
        robot.dialogService.sayReplic(text);
    };

    fn.closeDialog = function () {
        appData.modal.overlay.classList.remove('is-open');
        appData.modal.window.classList.remove('is-open');
        appData.modal.txt.textContent = '';

        removeSiblings(appData.modal.btns);
    };

    fn.createWindow = function (modalData, handler) {
        appData.modal.txt.textContent = modalData.text;

        if (modalData.answers) {
            removeSiblings(appData.modal.btns);

            let wideBtn = modalData.answers.length > 2;

            for (let val in modalData.answers) {

                let btn = document.importNode(document.querySelector('#tmpl-btn').content, true).querySelector('button');

                if (modalData.answers[val] === 'Нет') {
                    btn.classList.add(CLASSES.nope);
                }

                if (wideBtn) {
                    btn.classList.add(CLASSES.wide);
                }

                btn.textContent = modalData.answers[val];

                appData.modal.btns.appendChild(btn);

                btn.onclick = event => {
                    handler(btn.textContent);
                    robot.custom.send({"userClick": true});
                }
            }

            fn.openDialog(modalData.text);
        }
    };
}

//произнести реплику
function setTouchEvents(selector = '.js-action') {
    let elems = document.querySelectorAll(selector);

    for (let el of elems) {
        el.addEventListener('click', function () {
            let act = this.dataset.action;

            if (act) {
                robot.dialogService.sayReplic(act);

                robot.custom.send({"userClick": true});
            }
        })
    }
}

function addStartStopNavEvents() {
    const beginBtn = document.querySelector('#go-start');
    const finishBtn = document.querySelector('#go-end');

    beginBtn.addEventListener('click', function () {

        robot.dialogService.sayReplic('findout-visit-property');
        robot.custom.send({"caseFinished": true});
        robot.custom.send({"caseStarted": true});

        //очистка субтитров
        myApp.clearSub();

        //очистка рейтинга
        RobotRating.clearRate();

        myApp.statistic.user = null;
        myApp.statistic.rate = null;

    });

    finishBtn.addEventListener('click', function () {
        robot.dialogService.sayReplic('bye');
    })
}

function Rating() {
    let fn = this;
    let rate = document.querySelector('#rate');
    let btns = rate.querySelectorAll('.rate-item');

    fn.setRate = function () {
        for (let btn of btns) {
            btn.addEventListener('click', function () {
                if (fn.setSelected(this)) {
                    robot.custom.send({"userClick": true});

                    //это реплика-оценка
                    let rateTxt = btn.dataset.say;

                    myApp.statistic.rate = rateTxt.replace('bye-','');

                    //запись статистики
                    writeStatistic(myApp.statistic);

                    myApp.timeout.setRate = setTimeout(TIMEOUT.setRate.fn, TIMEOUT.setRate.t, rateTxt);
                }
            })
        }
    };

    fn.clearRate = function () {
        for (let btn of btns) {
            btn.classList.remove('is-selected');
        }
    };

    fn.setSelected = function (btn) {
        let selected = rate.querySelectorAll('.is-selected');

        if (selected.length) {
            return false;
        }

        btn.classList.add('is-selected');

        return true;
    };
}

function handleRate(rate) {
    let rateBtn = document.querySelector('[data-say=bye-' + rate + ']');
    rateBtn.click();
}

function writeStatistic(data) {
    robot.statistic(
        formatDate(new Date) + ";"
        + data.user + ";"
        + data.rate, "sale"
    )
}

document.addEventListener('DOMContentLoaded', function () {

    myApp.screens = document.querySelectorAll('.content-wrap');

    myApp.actionPlh = {
        title: document.querySelector('#screen-title'),
        plh1: document.querySelector('#plh1'),
        plh2: document.querySelector('#plh2')
    };

    initServicesPopup(myApp);
    setTouchEvents();
    addStartStopNavEvents();

    RobotRating = new Rating;

    RobotRating.setRate();

    myApp.ModalDialog = new ShowDialog(myApp);

    let robotInstance = 'getInstance';

    if(localTest){
        robotInstance = 'getStubInstance';
    }

    Promobot[robotInstance]().then((promobot) => {

        robot = promobot;

        promobot.userService.onUserPresence((user, isNew) => {
            console.log("is new", isNew, "user : ", user);
            myApp.statistic.user = user.user_id;
        });

        promobot.userService.onUserLost(no => {
            console.log("userLost");
        });

        promobot.dialogService.onUserReplic(text => {
            console.log("user said " + text);

            promobotOnUserSay(text);

        });

        promobot.custom.onEvent(data => {
            console.log("custom event", data);

            //case start
            if (data.startCase === true) {
                robot.dialogService.sayReplic('findout-visit-property');

                //очистка субтитров
                myApp.clearSub();

                //очистка рейтинга
                RobotRating.clearRate();

                myApp.statistic.user = null;
                myApp.statistic.rate = null;
            }

            //case finish
            if (data.stopCase === true) {
                robot.dialogService.sayReplic('bye');
            }
        });

        promobot.dialogService.onRobotReplicStart(text => {
            console.log("robot start replic " + text);

            promobotReplicaStart(text);
        });

        promobot.dialogService.onRobotReplicFinish(nothing => {
            console.log("robot finish replic ", nothing);

            promobotReplicaFinish();
        });

        promobot.dialogService.onQuestion(question => {
                myApp.ModalDialog.createWindow(question, answer => {
                    question.setAnswer(answer);
                    myApp.ModalDialog.closeDialog();
                });
            },
            nothing => {
                myApp.ModalDialog.closeDialog();
            }
        );

        promobot.dialogService.onAction(action => {
            console.log("onAction", action);

            promobotOnAction(action);
        })
    });

    myApp.muteRobot();
});

//--robot API events start-----

function promobotOnUserSay(text) {
    clearTimeout(myApp.timeout.clearSub);

    myApp.showSub(clearSubString(text));

    myApp.timeout.clearSub = setTimeout(TIMEOUT.clearSub.fn, TIMEOUT.clearSub.t);

    switch (myApp.curScreen) {
        case 'bye':
            stopTimeout('gotoMain', 'pause');
            break;
    }
}

function promobotReplicaStart(text) {
    myApp.showSub(clearSubString(text));

    clearTimeout(myApp.timeout.clearSub);

    switch (myApp.curScreen) {
        case 'bye':
            stopTimeout('gotoMain', 'pause');
            break;
    }

    if (localTest) {
        setTimeout(function () {
            robot.stub.robotReplicFinish();
        }, 2000);
    }
}

function promobotReplicaFinish() {

    myApp.clearSub();
    myApp.muteBtn.classList.remove('ico-mic-muted');

    clearTimeout(myApp.timeout.clearSub);

    switch (myApp.curScreen) {
        case 'bye':
            if (myApp.timeout.gotoMain === 'pause') {
                setTimeout(TIMEOUT.gotoMain.fn, TIMEOUT.gotoMain.t);
            }
            break;
    }
}

function promobotOnAction(action) {

    if (Array.isArray(action)) {

        for (let i = 0; i < action.length; i++) {
            if (action[i] === 'call-step-phrase') {
                let phrase = action[i + 1];

                console.log('onAction phrase', phrase);
                robot.dialogService.sayReplic(phrase);

                delete action[i];
                delete action[i + 1];
            }

            if (action[i] === 'screen') {
                let screenName = action[i + 1];

                console.log('onAction screen', screenName);

                myApp.curScreen = screenName;

                //при переходе на другой экран очищаются все запущенные таймеры
                for (let timer in myApp.timeout) {
                    stopTimeout(timer);
                }

                //и закрыть попап
                myApp.offersPopup.close();

                //открыть попап голосом
                if (screenName === 'popap') {
                    let popup = action[i + 2];
                    let slide = parseInt(document.querySelector(`[data-action=say_${popup}]`).dataset.index);

                    myApp.offersSlider.gotoSlide(slide);
                    myApp.offersPopup.open();

                    delete action[i];
                    delete action[i + 1];
                    delete action[i + 2];
                } else {
                    myApp.handleUserAction(screenName);

                    delete action[i];
                    delete action[i + 1];
                }
            }

            if (action[i] === 'rate') {
                console.log('onAction rate', action[i + 1]);
                handleRate(action[i + 1]);

                delete action[i];
                delete action[i + 1];
            }


            if (action[i] !== undefined) {
                console.log('onAction default', action[i]);
                myApp.handleUserAction(action[i + 1]);
            }
        }
    } else {
        myApp.handleUserAction(action);
    }
}

//--robot API events end-----

//--sale 1 start------
function initServicesPopup(mainObj = myApp) {
    let menu = document.querySelector('#start-menu');
    let btns = menu.querySelectorAll('.js-action');

    let slides = createSlider(btns);

    mainObj.offersPopup = new OffersPopup(mainObj);
    mainObj.offersSlider = new OffersSlider(slides);

    for(let i = 0; i < btns.length; i++){
        let btn = btns[i];

        btn.dataset.index = i;

        btn.addEventListener('click', function () {
            mainObj.offersSlider.gotoSlide(parseInt(this.dataset.index));
            mainObj.offersPopup.open();
        })
    }
}

function createSlider(items) {
    const IMGPATH = 'offers-pics/';
    let fragment = document.createDocumentFragment();

    for(let item of items){
        let slide = document
            .importNode(document.querySelector('#tmpl-offer-slide').content, true)
            .querySelector('.offer-slide');

        let imgName = item.dataset.picture;

        if (imgName < 10) {
            imgName = '0' + imgName;
        }

        slide.querySelector('.offer-slide__title').textContent = item.querySelector('.card__title').textContent;
        slide.querySelector('img').src = `${IMGPATH}${imgName}.png`;

        let btnInterest = slide.querySelector('.btn');

        btnInterest.addEventListener('click', function (e) {
            robot.custom.send({"userClick": true});

            robot.dialogService.sayReplic('consultant');

            if (localTest) {
                robot.stub.robotReplicStart('consultant');
            }

            e.preventDefault();
        });

        fragment.appendChild(slide);
    }

    return fragment;
}
//--sale 1 end------

function OffersSlider(items) {
    let fn = this;
    let slider = document.querySelector('#offers-slider');
    let sliderContent = slider.querySelector('.slider__content');
    sliderContent.appendChild(items);
    let slides = slider.querySelectorAll('.slide');
    let slidesNum = slides.length;

    let nav = createNav(slider);
    let navLeft = nav.left;
    let navRight = nav.right;

    if (slidesNum < 2) {
        navLeft.setAttribute('hidden', '');
        navRight.setAttribute('hidden', '');
    }

    checkNav(navLeft, navRight, 0);

    function checkNav(left, right, index) {
        if(index > 0 && index < slidesNum - 1){
            left.classList.remove('is-disabled');
            right.classList.remove('is-disabled');
        }else if(index === 0 && index < slidesNum - 1){
            left.classList.add('is-disabled');
            right.classList.remove('is-disabled');
        }else if(index > 0 && index === slidesNum - 1){
            left.classList.remove('is-disabled');
            right.classList.add('is-disabled');
        }
    }

    //TODO: выпилить это угребище
    fn.getCurIndex = function (slider) {
        let res = 0;


        let str = slider.style.transform;

        if (!str.length) {
            return res;
        }

        let n = str.indexOf("(");
        let n1 = str.indexOf(")");

        res = parseInt(str.slice(n + 1, n1 - 1));

        return Math.abs(res / 100);
    };

    fn.gotoSlide = function (index = 0) {

        checkNav(navLeft, navRight, index);

        sliderContent.style.transform = 'translateX(-' + (100 * index) + '%)';

        //сказать
        //robot.dialogService.sayText(sliderContent.children[index].dataset.say);

        // if (localTest) {
        //     console.log(sliderContent.children[index].dataset.say);
        // }
    };

    fn.removeSlides = function () {
        sliderContent.removeAttribute('style');
        removeSiblings(sliderContent);
        navRight.remove();
        navLeft.remove();
    };

    function createNav(wrapper) {
        let btnLeft = document.createElement('div');
        btnLeft.classList.add('slider__nav');
        let btnRight = btnLeft.cloneNode(false);
        btnRight.classList.add('slider__nav','nav-right');
        btnLeft.classList.add('nav-left');

        wrapper.appendChild(btnLeft);
        wrapper.appendChild(btnRight);

        return {
            'left': btnLeft,
            'right': btnRight
        }
    }

    navLeft.addEventListener('click', function () {
        let index = fn.getCurIndex(sliderContent);

        checkNav(navLeft, navRight, index);
        fn.gotoSlide(index - 1);
    });

    navRight.addEventListener('click', function () {
        let index = fn.getCurIndex(sliderContent);

        checkNav(navLeft, navRight, index);
        fn.gotoSlide(index + 1);
    });
}

function OffersPopup(mainObj) {
    let fn = this;

    let popup = document.querySelector('#offers-popup');
    let close = popup.querySelector('.popup-close');

    fn.open = function () {
        popup.classList.add('is-open');
        mainObj.modal.overlay.classList.add('is-open', 'is-dark');
    };

    fn.close = function () {
        popup.classList.remove('is-open');
        mainObj.modal.overlay.classList.remove('is-open', 'is-dark');
    };

    close.addEventListener('click', function () {
        fn.close();

        robot.dialogService.sayReplic('back-to-main');

        if (localTest) {
            robot.stub.robotReplicStart('back-to-main');
        }
    });
}

function stopTimeout(timeout, state = null) {
    console.log('stop timeout: ', timeout, myApp.timeout[timeout]);

    clearTimeout(myApp.timeout[timeout]);
    delete myApp.timeout[timeout];
    myApp.timeout[timeout] = state;
}

//--utils

function formatDate(date) {
    let day = date.getDate() + "";
    let month = (date.getMonth() + 1) + "";
    let year = date.getFullYear() + "";


    if (day.length < 2) {
        day = 0 + day;
    }

    if (month.length < 2) {
        month = 0 + month;
    }

    return day + "." + month + "." + year + " " + ('0' + date.getHours()).slice(-2) + ":" + ('0' + date.getMinutes()).slice(-2) + ":" + ('0' + date.getSeconds()).slice(-2);
}

function removeSiblings(parent) {
    while (parent.firstElementChild) {
        parent.firstElementChild.remove();
    }
}

function clearSubString(str) {
    if (str.length) {
        let clearStr = str.replace(/{[^}]+}/g, '');
        clearStr = clearStr.replace('+', '');
        clearStr = clearStr.replace(/ {1,}/g, " ");

        return clearStr;
    }
}

function findCharWidth(str) {
    let test = document.createElement('div');
    test.classList.add('char-test');
    test.textContent = str;
    document.body.appendChild(test);
    let w = test.clientWidth;
    test.remove();
    return w;
}

function findSubStringsRatio(str, charW, maxpx = 1000) {

    let strW = str.length * charW;

    return strW / maxpx;
}

function kitcut(text, limit) {
    if (text.length <= limit) return text;

    text = text.slice(0, limit);

    let lastSpace = text.lastIndexOf(" ");

    if (lastSpace > 0) {
        text = text.substr(0, lastSpace);
    }

    return text + "...";
}

//--utils