class PersonalOffers {

    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    get(phoneNumber, count) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {

                var data = [];
                for (var i = 0; i < count; i++) {
                    data.push({
                        "description": "Экономьте время при погашении кредитов различных Банков.",
                        "picture": "3",
                        "priority": "3",
                        "replic": i+" Услуга Автоплатеж на погашение кредита в другом банке позволит сохранить вышу кредитную историю безупречной. Вы будете проинформированы о сумме платежа перед списанием средств с вашей карты. В случае необходимости Вы сможете его отменить с помощью смс-сообщения. Подключаем?",
                        "title": "Автоматическое погашение кредитов "+i
                    })
                }
                let offersObj = {
                    "city": "Пермь",
                    "offers": data,
                    "user": "Ульяна Владимировна"
                };
                resolve(offersObj)
            }, 10);
        });
    }
}


