class PersonalOffers {

    constructor(baseUrl){
        this.baseUrl = baseUrl;
    }

    get(phoneNumber, count) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest()
            xhr.open("GET", this.baseUrl + "?phone="+phoneNumber.split("+").join(""), true);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        var response = xhr.responseText;
                        resolve(JSON.parse(response));
                    } else {
                        reject(xhr.status)
                    }
                }
            };
            
            xhr.send();

        });
    }
}
